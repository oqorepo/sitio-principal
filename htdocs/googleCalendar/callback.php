<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('google-calendar-api.php');
require_once('settings.php');

// Google passes a parameter 'code' in the Redirect Url
if(isset($_GET['code'])) {
	try {
		$date = $_SESSION['clinic']['date'];
		$date = explode('-', $date);

		$hour = explode(':', $_SESSION['clinic']['time']);
		$hourPLus = $hour[0]+1;
		
		$finishTime = $hourPLus.':'.$hour[1].':00';
		
		$startDate = $date[2].'-'.$date[1].'-'.$date[0].'T'.$_SESSION['clinic']['time'].':00-03:00';
		$finishDate = $date[2].'-'.$date[1].'-'.$date[0].'T'.$finishTime.'-03:00';

		$capi = new GoogleCalendarApi();
			
		// Get the access token 
		$data = $capi->GetAccessToken(CLIENT_ID, CLIENT_REDIRECT_URL, CLIENT_SECRET, $_GET['code']);
		$access_token = $data['access_token'];

		// Get user calendar timezone
		$user_timezone = $capi->GetUserCalendarTimezone($access_token);

		$calendar_id = 'primary';
		$event_title = 'Clinica Derco Motos';


		// Event starting & finishing at a specific time
		$full_day_event = 0; 
		$event_time = [ 'start_time' => $startDate, 'end_time' => $finishDate ];

		// Create event on primary calendar
		$event_id = $capi->CreateCalendarEvent($calendar_id, $event_title, $full_day_event, $event_time, $user_timezone, $access_token);

		if (isset($event_id['id'])) {
			header('Location:https://www.dercomotos.cl/clinica-de-manejo/paso-3?calendar=true');
		} else {
			header('Location:https://www.dercomotos.cl/clinica-de-manejo/paso-3?calendar=false');
		}
	}
	catch(Exception $e) {
		echo $e->getMessage();
		exit();
	}
}