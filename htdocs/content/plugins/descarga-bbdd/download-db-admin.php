<!DOCTYPE html>
<html>

<head>
    <title>Test</title>
    <link rel="stylesheet" href="<?php echo plugins_url() ?>/descarga-bbdd/jquery-ui/jquery-ui.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo plugins_url() ?>/descarga-bbdd/jquery-ui/jquery-ui.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
    $(function() {
        $("#datepicker").datepicker({dateFormat: "yy-mm-dd", maxDate: new Date, minDate: new Date(2007, 6, 12)});
        $("#datepicker2").datepicker({dateFormat: "yy-mm-dd", maxDate: new Date, minDate: new Date(2007, 6, 12)});
    });
    </script>
</head>

<body>
    <div class="container-fluid">
        <div class="row"></div>
        <div class="row">
            <div class="col-md-10">
                <h3>Descarga Cotizaciones</h3>
                <form name="newsletter" action="<?php echo plugins_url() ?>/descarga-bbdd/descarga/derco.php" method="POST">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Marca</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="marca">
                            <option value="ninguna">Todas</option>
                            <option value="suzuki">Suzuki</option>
                            <option value="zoongshen">Zongshen</option>
                            <option value="kymco">Kymco</option>
                            <option value="royal">Royal Enfield</option>
                        </select>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Fecha desde</span>
                        <input type="text" class="form-control" placeholder="fecha" aria-describedby="basic-addon2" id="datepicker" name="fechafrom" value="<?php echo date('Y-m-d', strtotime('-1 month')) ?>">
                        <span class="input-group-addon" id="basic-addon1">Fecha hasta</span>
                        <input type="text" class="form-control" placeholder="fecha" aria-describedby="basic-addon2" id="datepicker2" name="fechato" value="<?php echo date('Y-m-d'); ?>">
                    </div>
                    <small id="emailHelp" class="form-text text-muted">Ingrese ambas fechas</small>
                    <br><br>
                    <input type="submit" class="btn btn-primary" id="download" value="Descargar">
                </form>
            </div>
        </div>
    </div>
</body>

</html>
