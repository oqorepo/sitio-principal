<?php

/*

Plugin Name: Acceso a cotizaciones
Description: Este plugin permite acceder a los archivos CSV generador automaticamente con las cotizaciones del sitio
Author: Digital MEAT
Version: 0.1
Author URI: http://meat.cl

*/

require_once 'class/file.php';

add_action('admin_menu', 'csv_quotes_files');

function csv_quotes_files(){
	add_menu_page( 'Descargar Cotizaciones', 'Descargar Cotizaciones', 'read', 'csv-cotizaciones', 'view' );
}

$file = new file();

if(isset($_GET['file_id'])){
	$file->download($_GET['file_id']);
}

$AllFiles = $file->getAll();

function view(){
	global $current_user;

	if(($current_user->data->user_login == 'intouch' || $current_user->caps['administrator']) || $current_user->data->user_login == 'playlatam') {
		//Set var for view
		global $AllFiles;

		//Render view
		ob_start();
		include 'view.php';
		echo ob_get_clean();
	}

}