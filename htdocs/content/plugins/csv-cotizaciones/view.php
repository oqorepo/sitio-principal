<div class="wrap">
	<h1>Cotizaciones <a href="<?php echo get_site_url() . '/wp-admin/admin.php?page=csv-cotizaciones'; ?>" class="page-title-action">Actualizar</a></h1>
	<small>Los archivos con nombre <strong>AM</strong> son generados a las 11:00 AM y los con el nombre <strong>PM</strong> son generados a las 17:00 PM (GMT-4)</small>
	<table class="wp-list-table widefat fixed striped posts">
		<thead>
			<tr>
				<th scope="col" id="title" class="manage-column column-title column-primary">
					<span>Archivos</span>
				</th>
				<th>
					Fecha de archivo
				</th>
			</tr>
		</thead>
		<tbody id="the-list">
			<?php foreach ($AllFiles as $id => $file): ?>
			<tr>
				<td class="title column-title has-row-actions column-primary page-title" data-colname="Título">
					<strong>
						<a class="row-title" href="<?php echo get_site_url() . '/wp-admin/admin.php?page=csv-cotizaciones&file_id=' . $id ?>"><?php echo $file['name'] ?></a>
					</strong>
					<div class="locked-info">
						<span class="locked-avatar"></span>
						<span class="locked-text"></span>
					</div>
					<button type="button" class="toggle-row">
						<span class="screen-reader-text">Mostrar más detalles</span>
					</button>
				</td>
				<td><?php echo date('m/d/Y H:i:s', ($file['filemtime'] -10800 )) ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>