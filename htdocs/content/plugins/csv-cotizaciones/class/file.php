<?php

	class file{

		public function getAll(){
			
			$files = array();

			foreach (scandir(ABSPATH . '../../csv') as $file){
				if(strstr($file, '.csv')){
					$files[] = [
						'name' => $file,
						'path' => ABSPATH . '../../csv/' . $file,
						'filemtime' => filemtime(ABSPATH . '../../csv/' . $file)
					];
				}
			}
		
			//oreder by filemtime
			$sort_col = array();
			foreach ($files as $key=> $row) {
				$sort_col[$key] = $row['filemtime'];
			}
		
			array_multisort($sort_col, SORT_DESC, $files);
			
			return $files;
		}

		public function download($fileID){
				header("Content-Description: File Transfer");
				header("Content-Type: text/csv");
				header("Content-Disposition: attachment; filename=".basename($this->getAll()[$fileID]['name']));
				header("Content-Transfer-Encoding: Binary");
				header("Expires:0");
				header("Cache-control:must-revalidate");
				header("Pragma:public");
				flush();
				readfile($this->getAll()[$fileID]['path']);
				exit;
		}

	}