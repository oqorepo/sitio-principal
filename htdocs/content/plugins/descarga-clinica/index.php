<?php
/*
 Plugin Name: Descarga clinica
 Plugin URI: http://meat.cl
 Description: Transform JSON file to wordpress post
 Version: 0.1
 Author: Felipe Morano
 Author URI: http://felipemorano.com
 Text Domain: descarga-clinica
 */
/*  Copyright 2017	Felipe Morano  (email : fmorano@meat.cl)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

add_action('admin_menu', 'download_clinic_setup_menu');
 
function download_clinic_setup_menu(){
        add_menu_page( 'Inscritos Clinica', 'Inscritos Clinica', 'manage_options', 'descarga-clinica', 'clinic_init' );
}
 
function clinic_init(){
        require_once('download-clinica-admin.php');
 }
