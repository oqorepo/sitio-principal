<?php

if (!isset($_POST)) {
    header('Location:http://dercomotos.cl');
}
else{
    unlink('clinica.xls');
    /** Error reporting */
    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    date_default_timezone_set('Europe/London');

    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

    /** Include PHPExcel */
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require_once 'inc/conn.php';

    // Create new PHPExcel object
    //echo date('H:i:s') , " Create new PHPExcel object" , EOL;
    $objPHPExcel = new PHPExcel();

    // Set document properties
    //echo date('H:i:s') , " Set document properties" , EOL;
    $objPHPExcel->getProperties()->setCreator("")
                                 ->setLastModifiedBy("WEB")
                                 ->setTitle("Inscripciones")
                                 ->setSubject("Lista de inscritos")
                                 ->setDescription("")
                                 ->setKeywords("")
                                 ->setCategory("Lista");

    $cells = 'A1:U1';


    $style_header = array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb'=>'4869bd')
        )
    );
        $objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray( $style_header );

        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFont()->getColor()->applyFromArray(array("rgb" => 'FFFFFF'));


    // Start query to get fields from db to feed excel.
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'Nombre')
            ->setCellValue('C1', 'Apellido')
            ->setCellValue('D1', 'RUT')
            ->setCellValue('E1', 'Email')
            ->setCellValue('F1', 'Telefono')
            ->setCellValue('G1', 'Fecha')
            ->setCellValue('H1', 'Hora')
            ->setCellValue('I1', 'Dia de inscripción');

    // Set column width.
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);


    try {   
        $query = "SELECT * FROM wp_clinic_inscriptions WHERE inscription_date = '".$_POST['fechafrom']."'";

    	$qry1 = $conn->prepare($query);

    	$qry1->execute();

    	$resp = $qry1->fetchAll();

        $pos = 2;

    	foreach( $resp as $data ) {

            $objPHPExcel->setActiveSheetIndex(0)   
                ->setCellValue( 'A'.$pos, $data['id'] )
                ->setCellValue( 'B'.$pos, $data['name'] )
                ->setCellValue( 'C'.$pos, $data['lastname'] )
                ->setCellValue( 'D'.$pos, $data['rut'] )
                ->setCellValue( 'E'.$pos, $data['email'] )
                ->setCellValue( 'F'.$pos, $data['phone'] )
                ->setCellValue( 'G'.$pos, $data['inscription_date'] )
                ->setCellValue( 'H'.$pos, $data['inscription_hour'] )
                ->setCellValue( 'I'.$pos, $data['date'] );

            $pos++;
        }
    	
    } catch ( PDOException $e) {
    	echo $e->getMessage();
    }


    //echo date('H:i:s') , " Rename worksheet" , EOL;
    $objPHPExcel->getActiveSheet()->setTitle('Cotizacicones');


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Save Excel 2007 file
    /*echo date('H:i:s') , " Write to Excel2007 format" , EOL;
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
    echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;*/
    // Save Excel5 file
    //echo date('H:i:s') , " Write to Excel5 format" , EOL;
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save(str_replace('.php', '.xls', __FILE__));
    //echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;


    // Echo memory peak usage
    //echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

    // Echo done
    //echo date('H:i:s') , " Done writing files" , EOL;
    //echo 'Files have been created in ' , getcwd() , EOL;
    header('Location:'.str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME) . '?v=' . date('dmyhsi')));
}
?>
