<?php

class format {

	public static function number($number = 0) {
		return '$ ' . number_format($number, 0,',','.');
	}

	public static function title($string){
		$string = str_replace('-', ' ', $string);
		$string = ucfirst(strtolower($string));
		return $string;
	}

}