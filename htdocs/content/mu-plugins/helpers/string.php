<?php

function after($this, $inthat) {
	$return = false;
	if (!is_bool(strpos($inthat, $this)))
		$return = substr($inthat, strpos($inthat,$this)+strlen($this));

	return $return;
}

function before ($this, $inthat) {
	return substr($inthat, 0, strpos($inthat, $this));
}


function between ($this, $that, $inthat) {
	return before($that, after($this, $inthat));
}