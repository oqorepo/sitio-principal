<?php

class clear{

	public static function string($string) {

		$string = htmlspecialchars($string);

		return $string;
	}

	public static function stringInsert($string) {

		$search = array(
			"WHERE",
			"LIKE",
			"DELETE",
			"SELECT",
			"FROM",
			"DATABASE",
			"DATABASES",
			"FORCE",
			"TRUNCATE",
			"TABLE",
			"SQL",
			"script",
			"java",
			"applet",
			"iframe",
			"meta",
			"object",
			"html",
			"<",
			">",
			";",
			"'",
			"%"
		);

		$string = str_replace($search, " ", $string);
		$string = htmlspecialchars($string);
		$string = addslashes($string);

		return $string;
	}
	
	public static function onlyNumber($int){
		$int = intval($int);
		return preg_replace('([^0-9^])', '', $int);
	}
}