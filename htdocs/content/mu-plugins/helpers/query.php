<?php
	class query {

		public static function insert($table_name, $array_data){

			$data_last_value = implode('', array_slice($array_data, -1));
			$query_key = '';
			$query_value = '';

			foreach ($array_data as $key => $value){
				$query_key .= '`'. $key. '`';
				if($data_last_value !== $value)
					$query_key .= ', ';
			}

			foreach ($array_data as $key => $value){
				$query_value .= "'" . $value . "'";
				if($data_last_value !== $value)
					$query_value .= ', ';
			}

			return "INSERT INTO $table_name ($query_key) VALUES ($query_value);";
		}
	}