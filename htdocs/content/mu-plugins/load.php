<?php

/*
Plugin Name: Mu-Plugins Loader
Description: Simple loader in order to run the Themosis framework core files.
Author: Julien Lambé
Version: 1.0
Author URI: http://framework.themosis.com/
*/

require_once(WPMU_PLUGIN_DIR.'/themosis-framework/themosis.php');
require_once(WPMU_PLUGIN_DIR.'/bfi-thumb/bfi-thumb.php');
require_once(WPMU_PLUGIN_DIR.'/bfi-thumb/bfi-thumb-controller.php');
require_once(WPMU_PLUGIN_DIR.'/helpers/clear.php');
require_once(WPMU_PLUGIN_DIR.'/helpers/validate.php');
require_once(WPMU_PLUGIN_DIR.'/helpers/format.php');
require_once(WPMU_PLUGIN_DIR.'/wp-query-multisite/wp-query-multisite.php');
require_once(WPMU_PLUGIN_DIR.'/helpers/string.php');
require_once(WPMU_PLUGIN_DIR.'/helpers/query.php');