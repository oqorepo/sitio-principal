<?php

/**
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */

Route::get('page', function()
{
    return View::make('marcas.page.page');
});

Route::get('template', ['inicio-marcas', function()
{
    return View::make('marcas.page.inicio');
}]);

Route::get('template', ['concesionarios-marcas', function()
{
	global $post;
	//var_dump($post->post_parent);
	//die('hola');
	return View::make('marcas.page.concesionarios', [
		'post_parent'   => $post->post_parent
	]);
}]);

Route::get('singular', array('modelos', function()
{
	return View::make('marcas.page.single-modelos');
}));

Route::get('template', ['timeline', function()
{
    return View::make('marcas.page.timeline');
}]);

Route::get('template', ['contacto-marcas', function()
{
    return View::make('marcas.page.contacto');
}]);

Route::get('template', ['novedades', function()
{
	return View::make('marcas.page.novedades');
}]);

Route::get('template', ['modelos', function()
{
	return View::make('marcas.page.modelos');
}]);

Route::get('template', ['videos', function()
{
    return View::make('marcas.page.video');
}]);

Route::get('template', ['experience', function()
{
    return View::make('marcas.page.experience');
}]);

Route::get('singular', ['modelo', function()
{
	return View::make('marcas.page.single-modelos');
}]);

Route::get('template', ['cotizar', 'uses' => 'BaseController@cotizar']);

Route::get('tax', function()
{
	global $wp_query;
	return View::make('marcas.page.modelo', [
		'taxonomy_term'     => $wp_query->queried_object->slug,
		'taxonomy_title'    => $wp_query->queried_object->name
	]);
}); 

Route::get('single', function()
{
	return View::make('marcas.page.single');
});
	
Route::get('404', function()
{
	return View::make('marcas.page.404');
});
