<?php
// allow SVG in media
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function acf_load_color_field_choices( $field ) {
    // reset choices
    $field['choices'] = array();

    if (SITE_NAME == 'suzuki') {
	    $field['choices'] = [
	    	'revoluciones' => 'Asistencia a bajas revoluciones',
			'control' => 'Control de partida',
			'traccion' => 'Control de Tracción',
			'abs' => 'Frenos ABS',
			'electronica' => 'Inyección Electrónica',
			'euro3' => 'Norma Euro 3',
			'euro4' => 'Norma Euro 4',
			'quickshift' => 'Quick Shift Bidireccional',
			'antirrobo' => 'Sistema Antirrobo',
			'easystart' => 'Sistema Easy Start',
			'ramair' => 'Sistema Ram Air',
	    ];
	} elseif (SITE_NAME == 'kymco') {
		$field['choices'] = [
			'4x4' => '4x4',
			'bloqueo' => 'Bloqueo de diferencial',
			'compartimiento' => 'Compartimiento móviles',
			'abs' => 'Frenos ABS',
			'huinche' => 'Huinche',
			'electronica' => 'Inyección Electrónica',
			'aleacion' => 'Llantas de aleación',
			'euro3' => 'Norma Euro 3',
			'euro4' => 'Norma Euro 4',
			'12v' => 'Puerto 12v',
			'usb' => 'Puerto USB'
		];
	} elseif (SITE_NAME == 'royal') {
		$field['choices'] = [
			'abs' => 'Frenos ABS',
			'electronica' => 'Inyección Electrónica',
			'euro3' => 'Norma Euro 3',
			'euro4' => 'Norma Euro 4',
		];
	} elseif(SITE_NAME == 'zongshen'){
		$field['choices'] = [
			'12v' => 'Puerto 12v',
			'alarma' => 'Alarma',
			'electronica' => 'Inyección Electrónica',
			'euro3' => 'Norma Euro 3',
			'euro4' => 'Norma Euro 4',
			'remoto' => 'Sistema de partida remoto',
			'usb' => 'Puerto USB'
		];
	}

    // return the field
    return $field;
}

add_filter('acf/load_field/name=mostrar_icono_de', 'acf_load_color_field_choices');
