<?php
	
	Ajax::run('ftp-upload', 'both', function(){
		// temporary fix
		die();

		global $wpdb;
		$csv_name = 'cotizaciones-'. SITE_NAME .'-'. date('Y-m-d-A').'.csv';
		$csv_path = ABSPATH . '/../../csv/';
		$csv = fopen($csv_path . $csv_name, "w+") or die("Unable to open file!");

		$fields = [
			'id',
			'fecha',
			'simulador_clientes_rut',
			'nombre',
			'apellido',
			'fonoFijo',
			'fonoMovil',
			'mail',
			'calle',
			'numero',
			'comuna',
			'ciudad',
			'region',
			'origen',
			'origen2',
			'marca',
			'modelo',
			'version',
			'codigoVersion',
			'versionColor',
			'accesorios',
			'precioVersion',
			'partePagoMarca',
			'partePagoModelo',
			'partePagoPatente',
			'partePagoAnio',
			'partePagoValor',
			'Nombre Ces',
			'Direccion Ces',
			'N Direccion Ces',
			'Codigo Concesionario',
			'Centro Concesionario',
			'codigoConcesionario (DEALER PORTAL)',
			'centroConcesionario(DEALER PORTAL)'
		];

		fwrite($csv, implode("|", $fields));

		$table = $wpdb->prefix . 'quotes';

		$results = $wpdb->get_results( "SELECT * FROM $table WHERE `export` = '0'", OBJECT );

		foreach ($results as $result){
			$values = [
				$result->id, //id
				date_format(date_create($result->creation_date), 'd-m-Y H:i'), // fecha
				$result->rut, // simulador_clientes_rut
				$result->firstname, // nombre
				$result->lastname, // apellido
				$result->phone, // fonoFijo
				$result->phone, // fonoMovil
				$result->email, // mail
				$result->address, // calle
				'', // numero
				$result->comuna_personal, // comuna
				'', // ciudad
				$result->region_personal, // region
				$result->url_cotizacion, // origen
				get_site_url(), // origen2
				SITE_NAME, // marca
				$result->model, // modelo
				$result->model_number, // version
				'', // codigoVersion
				'', // versionColor
				'', // accesorios
				format::number($result->value_total), // precioVersion
				'', // partePagoMarca
				'', // partePagoModelo
				'', // partePagoPatente
				'', // partePagoAnio
				'', // partePagoValor
				$result->concessionaire_name, // Nombre Ces
				'', // Direccion Ces
				'Cotización Remota', // N Direccion Ces
				'', // Codigo Concesionario
				'', // Centro Concesionario
				'', // codigoConcesionario (DEALER PORTAL)
				'' // centroConcesionario(DEALER PORTAL)
			];
			fwrite($csv, "\n" . implode("|", $values));

		}

		fwrite($csv, "\n");
		fclose($csv);

		# Upload FTP file
		$conn_id = ftp_connect(getenv('FTP_CSV_SERVER'));

		ftp_login($conn_id, getenv('FTP_CSV_USER'), getenv('FTP_CSV_PASSWORD'));

		if (ftp_put($conn_id, '/meat/Derco/cotizaciones/'. $csv_name, $csv_path . $csv_name, FTP_ASCII))
			echo "se ha cargado con éxito.";

		else
			echo "Hubo un problema durante la transferencia.";

		ftp_close($conn_id);


		$wpdb->update(
			$table,
			['export' => '1'],
			['export' => '0']
		);

		echo 'Update CSV file :) ';
	});