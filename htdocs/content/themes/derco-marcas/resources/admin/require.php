<?php
	
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/0-site-detect.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/session.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/acf.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/clear-title.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/assets.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/helpers.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/ajax-cities.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/application.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/ajax-contacto.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/ajax-concesionarios.php';
	require ABSPATH.'/../content/themes/derco-motos/resources/admin/pdf.php';
