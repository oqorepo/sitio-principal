<?php

PostType::make('modelo', 'Modelos', 'Modelo')->set([
	'public'        => true,
	'menu_position' => 20,
	'supports'      => ['title', 'editor', 'revisions'],
	'rewrite'       => true,
	'query_var'     => false,
	'menu_icon'     => 'dashicons-portfolio'
]);

Taxonomy::make('tipos_modelos', 'modelo', 'Tipos de Modelos', 'Tipo de Modelo')->set([
	'public'            => true,
	'query_var'         => false,
	'hierarchical'      => true,
	'show_admin_column' => true,
	'rewrite'           => array( 'slug' => 'modelos' )
]);

//require_once ABSPATH.'/../content/themes/derco-motos/resources/admin/custom-post-modelos.php';