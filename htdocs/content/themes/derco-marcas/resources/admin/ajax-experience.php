<?php

	Ajax::run('experience', 'both', function(){

		global $wpdb;
		
		$mandrill = new Mandrill('anCnzLBpxLE5XvzJw3Togg');
		$table = $wpdb->prefix . 'clinic';

		if(!isset($_POST['fullname']))
			$_POST['fullname'] = $_POST['firstname'] .' '. $_POST['lastname'];

		$array_data = [
			'fullname'  => $_POST['fullname'],
			'rut'       => $_POST['rut'],
			'email'     => $_POST['email'],
			'phone'     => substr($_POST['phone'], -9)
		];


		$message = [
			'from_email' 		  => 'no-reply@dercomotos.cl',
			'from_name' 		  => 'DERCO Motos',
			'subject' 			  => 'Registro Suzuki Experience',
			'preserve_recipients' => FALSE,
		];

		$message['to'] = [
			['email' => 'mjyanez@digitalmeat.cl'],
		];

		$vars_for_template = [
			'FORMULARIO'	=> 'Suzuki Experience',
			'FIRSTNAME' => $array_data['fullname'],
			'RUT' => $array_data['rut'],
			'EMAIL_USER' => $array_data['email'],
			'PHONE' => $array_data['phone'],
			'MENSAJE'	=> 'Registro Suzuki Experience'
		];

		foreach($vars_for_template as $key => $value):
			$message["global_merge_vars"][] = [
				'name' => $key,
				'content' => $value
			];
		endforeach;

		#Save in DB
		$wpdb->query(query::insert($table, $array_data));		

		#Send Mail
		$mandrill->messages->sendTemplate('derco-formularios', [] ,$message);


		$response = [
			'status' => true,
			'message' => [
				'title' => $_POST['fullname'] .', gracias por contactarnos',
				'text' => $_POST['send_text']
			]
		];

		wp_send_json($response);

	});