<?php

Ajax::run('cotizacion', 'both', function(){

	#Define Vars
	global $wpdb;
	$table = 'QUOTES';
	$mandrill = new Mandrill('anCnzLBpxLE5XvzJw3Togg');

	if(empty($_POST['value_total'])):
		$value_bono = 0;
		$value_total = 0 . $_POST['value_vehicle'];  //no quitar '0', es necesario para usar query::insert()
	else:
		$value_bono = $_POST['value_bono'];
		$value_total = $_POST['value_total'];
	endif;

	$pdf_file_name = helpers::randomString(20);
	$pdf_file_url = get_site_url() . '/pdf/' . $pdf_file_name . '.pdf';

	$array_data = [
		'firstname'             => $_POST['firstname'],
		'lastname'              => $_POST['lastname'],
		'rut'                   => $_POST['rut'],
		'email'                 => $_POST['email'],
		'phone'                 => substr($_POST['phone'], -9),
		'address'               => isset($_POST['address']) ? $_POST['address'] : '',
		'comuna_personal'       => $_POST['comuna_personal'],
		'region_personal'       => $_POST['region_personal'],
		'model'                 => $_POST['model'],
		'model_number'          => $_POST['model_number'],
		'concessionaire_name'   => isset($_POST['concessionaire']) ? explode('|', $_POST['concessionaire'])[1] : '',
		'concessionaire_comuna' => isset($_POST['comuna_concessionaire']) ? $_POST['comuna_concessionaire'] : '',
		'concessionaire_region' => isset($_POST['region_concessionaire']) ? $_POST['region_concessionaire'] : '',
		'value_vehicle'         => $_POST['value_vehicle'],
		'value_bono'            => $value_bono,
		'value_total'           => $value_total,
		'url_cotizacion'        => get_site_url() . $_POST['url_cotizacion'],
		'pdf_url'               => $pdf_file_url,
		'export'                => '0'
	];

	if(SITE_NAME == 'royal') {
		$site = 'Royal Enfield';
	} else {
		$site = SITE_NAME;
	}

	$message = [
		'from_email' => 'no-reply@dercomotos.cl',
		'from_name' => 'DERCO Motos',
		'subject' => 'Cotización moto modelo: '. $array_data['model'] .' - '. strtoupper($site),
		'preserve_recipients' => FALSE,
	];

	if(getenv('ENVIRONMENT') == 'production'){
		$message['to'] = [
			['email' => $array_data['email']], // email cliente
			['email' => isset($_POST['concessionaire']) ? explode('|', $_POST['concessionaire'])[0] : ''], // email concesionario
			['email' => isset($_POST['concessionaire']) ? explode('|', $_POST['concessionaire'])[2] : ''], // email concesionario
			['email' => isset($_POST['concessionaire']) ? explode('|', $_POST['concessionaire'])[3] : ''], // email concesionario
			['email' => isset($_POST['concessionaire']) ? explode('|', $_POST['concessionaire'])[4] : ''] // email concesionario
		];
	} else {
		$message['to'] = [
			['email' => $array_data['email']]
		];
	}

	foreach (get_field('destinatarios_contacto', 'option') as $email){
		$message['to'][] = $email; // email opciones transversales
	}

	$model = empty($array_data['model_number']) ?  " " . $array_data['model'] . " " : $array_data['model'] . " (" . $array_data['model_number']. ") ";

	$vars_for_template = [
		'FIRSTNAME' => $array_data['firstname'],
		'LASTNAME' => $array_data['lastname'],
		'RUT' => $array_data['rut'],
		'EMAIL_USER' => $array_data['email'],
		'PHONE' => $array_data['phone'],
		'ADDRESS' => $array_data['address'],
		'COMUNA_PERSONAL' => '---',
		'REGION_PERSONAL' => '---',
		'MARCA' => strtoupper($site),
		'MODEL' => $model,
		'CONCESSIONAIRE_NAME' => $array_data['concessionaire_name'],
		'CONCESSIONAIRE_COMUNA' => $array_data['concessionaire_comuna'],
		'VALUE_VEHICLE' => format::number($array_data['value_vehicle']),
		'VALUE_BONO' => format::number($array_data['value_bono']),
		'VALUE_TOTAL' => format::number($array_data['value_total']),
		'URL_COTIZACION' => $array_data['url_cotizacion'],
		'PDF_URL' => $pdf_file_url
	];

	foreach($vars_for_template as $key => $value):
		$message["global_merge_vars"][] = [
			'name' => $key,
			'content' => $value
		];
	endforeach;

	#Save in DB
	if ($wpdb->query(query::insert($table, $array_data)) ){
		// Save file to FTP ///////////////////////////////
		global $wpdb;

		// get ID of row from current inserted query
		$lastid = $wpdb->insert_id;

		function generateRandomString($length = 10) {
		    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < $length; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    return $randomString;
		}

		$rel = generateRandomString(5);
		// MARCA_AAAAMMDD_XXX.CSV
		$csv_name = SITE_NAME .'_'. date('Ymd') .'_'. $rel .'.csv';
		$csv_path = ABSPATH . '/../../csv/';

		$csv = fopen($csv_path . $csv_name, "w+") or die("Unable to open file!");

		$fields = [
			'id',
			'fecha',
			'simulador_clientes_rut',
			'nombre',
			'apellido',
			'fonoFijo',
			'fonoMovil',
			'mail',
			'calle',
			'numero',
			'comuna',
			'ciudad',
			'region',
			'origen',
			'origen2',
			'marca',
			'modelo',
			'version',
			'codigoVersion',
			'versionColor',
			'accesorios',
			'precioVersion',
			'partePagoMarca',
			'partePagoModelo',
			'partePagoPatente',
			'partePagoAnio',
			'partePagoValor',
			'Nombre Ces',
			'Direccion Ces',
			'N Direccion Ces',
			'Codigo Concesionario',
			'Centro Concesionario',
			'codigoConcesionario (DEALER PORTAL)',
			'centroConcesionario(DEALER PORTAL)'
		];

		fwrite($csv, implode("|", $fields));

		$table = $wpdb->prefix . 'quotes';

		$values = [
			$lastid, //id
			date_format(date_create($result->creation_date), 'd-m-Y H:i'), // fecha
			$array_data['rut'], // simulador_clientes_rut
			$array_data['firstname'], // nombre
			$array_data['lastname'], // apellido
			substr($array_data['phone'], -9), // fonoFijo
			substr($array_data['phone'], -9), // fonoMovil
			$array_data['email'], // mail
			'', // calle
			'', // numero
			$array_data['comuna_personal'], // comuna
			'', // ciudad
			$array_data['region_personal'], // region
			$array_data['url_cotizacion'], // origen
			get_site_url(), // origen2
			SITE_NAME, // marca
			$array_data['model'], // modelo
			$array_data['model_number'], // version
			'', // codigoVersion
			'', // versionColor
			'', // accesorios
			format::number($value_total), // precioVersion
			'', // partePagoMarca
			'', // partePagoModelo
			'', // partePagoPatente
			'', // partePagoAnio
			'', // partePagoValor
			$array_data['concessionaire_name'], // Nombre Ces
			'', // Direccion Ces
			'Cotización Remota', // N Direccion Ces
			'', // Codigo Concesionario
			'', // Centro Concesionario
			'', // codigoConcesionario (DEALER PORTAL)
			'' // centroConcesionario(DEALER PORTAL)
		];

		fwrite($csv, "\n" . implode("|", $values));


		fwrite($csv, "\n");
		fclose($csv);

		# Upload FTP file
		$conn_id = ftp_connect(getenv('FTP_CSV_SERVER'));

		$login = ftp_login($conn_id, getenv('FTP_CSV_USER'), getenv('FTP_CSV_PASSWORD'));

		$put = ftp_put($conn_id, '/meat/Derco/cotizaciones-pp/'. $csv_name, $csv_path . $csv_name, FTP_ASCII);

		if ($array_data['email'] == 'fmorano@meat.cl') {
			tp($login);
			tp($put);
		}
		ftp_close($conn_id);


		$wpdb->update(
			$table,
			['export' => '1'],
			['export' => '0']
		);
	}


	#Send Mail
	if ($array_data['email'] != 'fmorano@meat.cl') {
		$mandrill->messages->sendTemplate(SITE_NAME . '-motos-cotizacion', [] ,$message);
	}


	#Save PDF
	ob_start();

	if(SITE_NAME == 'suzuki'){
		$url_logo = 'https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/13592ac2-7f97-4f2b-8b17-14d6cbc212c4.png';
		$width_logo = '120';
		$color_top = 'white';
		$color_bottom = '#3a66c8';
	} elseif(SITE_NAME == 'zongshen') {
		$url_logo = 'https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/54d0b4a4-f139-48df-b9fe-7306a12c7551.png';
		$width_logo = '250';
		$color_top = '#ff0000';
		$color_bottom = '#ff0000';
	} elseif(SITE_NAME == 'kymco') {
		$url_logo = 'https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/7603e24a-e87b-4bb6-b257-fa8244af5fca.png';
		$width_logo = '250';
		$color_top = 'white';
		$color_bottom = '#d00429';
	} elseif(SITE_NAME == 'royal') {
		$url_logo = 'https://www.dercomotos.cl/content/uploads/sites/5/bfi_thumb/royal-logo-1-ncz3j6h49havvv9dxhpnmt33uhub2dkvvtxogabwzk.png';
		$width_logo = '250';
		$color_top = 'white';
		$color_bottom = '#000';
	} else {
		$url_logo = 'https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/394198e5-71ab-42de-b997-734079c306d0.png';
		$width_logo = '200';
		$color_top = '#ee1a2e';
		$color_bottom = '#ee1a2e';
	}

	include ABSPATH.'../content/themes/derco-motos/pdf-template/quote.php';

	$content = ob_get_clean();
	$html2pdf = new Html2Pdf('P', 'LETTER', 'es');
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->writeHTML($content);
	$html2pdf->Output(ABSPATH . '../pdf/' . $pdf_file_name . '.pdf', 'F');
	
	#Set var session
	$_SESSION['user'] = [
		'firstname'     => $array_data['firstname'],
		'lastname'      => $array_data['lastname'],
		'rut'           => $array_data['rut'],
		'email'         => $array_data['email'],
		'phone'         => $array_data['phone'],
		'address'       => $array_data['address'],
		'region_personal'   => $array_data['region_personal'],
		'comuna_personal'   => $array_data['region_personal']
	];

	
	
	#Define JSON
	$response = [
		'status' => true,
		'message' => [
			'title' => 'Cotización',
			'text' => 'Hemos recibido con éxito su cotización, pronto nos pondremos en contacto'
		],
		'response' => [
			'fullname' => $array_data['firstname'] .' '. $array_data['lastname'],
			'model' =>  $model,
			//'email' => isset($_POST['concessionaire']) ? strtolower(explode('|', $_POST['concessionaire'])[0]) :  '',
			'pdf' => $pdf_file_url
		],
	];

	
	#Response JSON
	wp_send_json($response);

});