<?php

if( function_exists('acf_add_options_page') ) {
	$page = acf_add_options_page(array(
		'page_title' => 'Opciones Transversales',
		'menu_title' => 'Opciones Transversales',
		'menu_slug' => 'opciones-transversales',
		'capability' => 'edit_posts',
		'position' => 10,
		'redirect' => false,
		'icon_url' => 'dashicons-align-right'
	));

		//Sub page
		acf_add_options_sub_page(array(
			'title' => 'Datos de contacto marca',
			'page_title' => 'Datos de contacto marca',
			'parent' => 'opciones-transversales',
			'capability' => 'manage_options'
		));

		acf_add_options_sub_page(array(
			'title' => 'Footer',
			'page_title' => 'Footer',
			'parent' => 'opciones-transversales',
			'capability' => 'manage_options'
	));
}