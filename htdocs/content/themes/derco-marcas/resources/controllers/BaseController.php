<?php

class BaseController extends Controller
{
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout))
        {
            $this->layout = Pronto::make($this->layout);
        }
    }

    /*protected function taxonomyTiposModelos() {

		$term = explode('/', $_SERVER['REQUEST_URI']);
		$term = $term[(count($term)) -2];

		if(term_exists($term,'tipos_modelos')):
			http_response_code(200);
			$custom_title = format::title($term) . ' - Modelos';
			return View::make('marcas.page.modelo', [
				'title'     => $custom_title,
				'type'      => $term
			]);

		else:
			header("HTTP/1.1 404 Not Found");
			header("Location: " . get_bloginfo('url'));
			exit;
		endif;
	}*/
	
	protected function cotizar(){
		
		if(isset($_GET['model']))
			$model = $_GET['model'];
		elseif(isset($_COOKIE['model_select']))
			$model = $_COOKIE['model_select'];
		else
			$model = 0;

		return View::make('marcas.page.cotizar', [
			'model' => clear::onlyNumber($model)
		]);

	}
}