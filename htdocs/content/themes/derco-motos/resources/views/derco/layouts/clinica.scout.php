<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Default Meta -->
    <title>Derco Motos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="DercoMotos">
    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ themosis_assets() }}/favicon-derco/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ themosis_assets() }}/favicon-derco/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ themosis_assets() }}/favicon-derco/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ themosis_assets() }}/favicon-derco/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ themosis_assets() }}/favicon-derco/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ themosis_assets() }}/favicon-derco/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ themosis_assets() }}/favicon-derco/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ themosis_assets() }}/favicon-derco/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ themosis_assets() }}/favicon-derco/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ themosis_assets() }}/favicon-derco/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ themosis_assets() }}/favicon-derco/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ themosis_assets() }}/favicon-derco/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ themosis_assets() }}/favicon-derco/favicon-16x16.png">
    <link rel="manifest" href="{{ themosis_assets() }}/favicon-derco/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ themosis_assets() }}/favicon-derco/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Style -->
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="{{ themosis_assets() }}/fonts/stylesheet.css" rel="stylesheet" type="text/css">
    <!-- Scripts -->
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAtCl9KwsDj9abVk-ORpT5GRtAsePghcs0"></script>
    <!-- AIzaSyAtCl9KwsDj9abVk-ORpT5GRtAsePghcs0-->
    <!--[if lt IE 9]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
  </head>

<body style="display:none;">
    <div id="derco">
      <div id="barmenu"><a class="trigger">Trigger<span class="box"> <span class="bar top"></span><span class="bar middle"></span><span class="bar bottom"></span></span></a>
        <nav role="navigation">
          <div class="logo"> <a href="index.html"><img src="{{ themosis_assets() }}/img/logo-derco-motos.png" alt="Derco Motos"></a></div>
          <div class="scrolling">
            <div class="login">
               <form action="{{ helpers::ajax_url() }}" class="form-clinic" method="POST">
                  <input type="hidden" name="action" value="edit">
                <div class="form-fields">
                  <div class="fields"> 
                    <div class="field"> 
                      <div class="control rut rut">
                        <label for="rut" class="control-label">Rut</label>
                        <input class="control-input rut" type="text" id="rut" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido"/>
                      </div>
                    </div>
                  </div>
                  <div class="fields">
                    <div class="control"> 
                      <input type="submit" value="Revisar" role="form" class="control-submit"/>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="buttons"><a href="#">Volver a dercomotos</a></div>
            <div class="social">
              <nav>
                <ul> 
                  @if(get_field('redes_sociales', 'option'))
                      @while(has_sub_field('redes_sociales', 'option'))
                          <li><a href="{{get_sub_field('url', 'option')}}" target="_blank" title="{{get_sub_field('titulo', 'option')}}" class="social facebook">{{get_sub_field('icono', 'option')}}</a></li>
                      @endwhile
                  @endif
                </ul>
              </nav>
            </div>
          </div>
        </nav>
      </div>
      <header> 
        <div class="header">
          <div class="header-clinic">
            <div class="inner"> 
              <div class="container">
                <div class="row">
                  <div class="col-md-4 hidden-sm hidden-xs">
                    <div class="clinic-back"> <a href="http://dercomotos.cl"><i class="material-icons">arrow_back</i><span>Volver a derco motos</span></a></div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="clinic-logo"> <a href="http://dercomotos.cl"><img src="{{ themosis_assets() }}/img/logo-derco-motos.png" alt="Derco Motos"></a></div>
                  </div>
                  <div class="col-md-4 col-sm-6 hidden-xs">
                    <div class="clinic-login"> <a href="#"><span>Revisar reserva</span><i class="material-icons">check</i></a>
                      <div class="reservation">
                        <form action="{{ helpers::ajax_url() }}" class="form-clinic" method="POST">
                            <input type="hidden" name="action" value="edit">
                          <div class="form-fields">
                            <div class="fields"> 
                              <div class="field"> 
                                <div class="control rut rut">
                                  <label for="rut" class="control-label">Rut</label>
                                  <input class="control-input rut" type="text" id="rut" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido"/>
                                </div>
                              </div>
                            </div>
                            <div class="fields errors">
                              <div class="control-error">
                                <div class="errors">
                                  <div class="inner"> 
                                    <ul> </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="fields">
                              <div class="control"> 
                                <input type="submit" value="Revisar" role="form" class="control-submit"/>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div id="page"> 
        <div class="page-content">
          <div class="page-home">
                    
                    @yield('content')

                </div>
            </div>
        </div>
        <div id="quotation"><a href="cotizar.html"><span class="icon"> <i class="fa fa-usd"></i></span><span class="text">Cotiza tu moto</span></a></div>
        
        @include('derco.section.footer')

    </div>
    <div id="loading"> <i class="fa fa-spinner fa-spin"> </i></div>

    @yield('modal')

</body>

</html>