@extends('derco.layouts.main')

@section('main')

<div id="page">
	<div class="page-content">
		<div class="page-home">
			@include('derco.section.slide')
			<section class="home-trademarks">
				<div class="background"></div>
				<div class="content">
					<div class="inner">
						<div class="limit">
							<div class="items">
								
								@if(get_field('marcas'))
									@while(has_sub_field('marcas'))

										<div class="item animate">
											<div class="picture get-image">
												<a href="{{get_sub_field('url')}}" class="image"  target="_blank">
													<img src="{{bfiThumb::always(get_sub_field('imagen_destacada')['url'], ['width' => 370, 'height' => 200] )}}" alt="{{get_sub_field('imagen_destacada')['alt']}}">
												</a>
											</div>
											<div class="info">
												<div class="inner">
													<div class="title" data-equalize="picture-branding">
														<h2>{{get_sub_field('logo_marca')['alt']}}</h2>
														<a href="{{get_sub_field('url')}}" target="_blank">
															<img src="{{get_sub_field('logo_marca')['url']}}" alt="{{get_sub_field('logo_marca')['alt']}}">
														</a>
													</div>
													<div data-equalize="home-trademarks-excerpt" class="excerpt">
														<p>{{get_sub_field('descripcion')}}</p>
													</div>
													<div class="buttons">
														<a href="{{get_sub_field('url')}}" class="button red" target="_blank">{{{get_sub_field('texto_boton')}}}</a>
													</div>
												</div>
											</div>
										</div>

									@endwhile
								@endif

							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="home-news">
				<div class="background"></div>
				<div class="content">
					<div class="inner">
						<div class="limit">
							<div class="title">
								<h1 class="title-line">Noticias</h1>
							</div>
							<div class="items">
							<div class="container">
                    		<div class="row">
							<?php

								$args = [
									'post_type' => 'post',
									'posts_per_page' => 6,
									//'category_name' => 'noticia-destacada',
									'sites' => [ 'sites__in' => [ 1, 2, 3, 4 ] ]
								];

								$i = 0;

								$query = new WP_Query_Multisite($args); while( $query->have_posts() ) : $query->the_post();

							?>
								<div class="col-lg-4 col-sm-6">
									<article class="item animate">
												<?php
														$params = ['width' => 280, 'height' => 330];
												?>
											<div  class="picture get-image">
												<a href="{{get_the_permalink()}}" class="image">
													<img src="{{bfiThumb::always(get_field('imagen_compartir')['url'], $params)}}" alt="{{get_field('imagen_compartir')['alt']}}">
												</a>
											</div>
											<div class="info">
												<div class="inner">
													<div class="date">
														<i class="fa fa-clock-o"></i>{{{get_the_date()}}}
													</div>
													<h2 data-equalize="home-news-item-item">{{{get_the_title()}}}</h2>
													<div class="excerpt">
														<p>{{get_field('bajada')}}</p>
													</div>
													<div class="buttons">
														<a href="{{get_the_permalink()}}"><i class="fa fa-plus"> </i>Ver más</a>
													</div>
												</div>
											</div>
									</article>
								</div>

								<?php $i++; endwhile; wp_reset_postdata(); ?>

							</div>
							</div>
							</div>
							<div class="buttons"> <a href="{{get_site_url('', '?page_id=69')}}" class="button">Ver todas las noticias</a></div>
						</div>
					</div>
				</div>
			</section>
			<section class="home-clinic">
              <div class="content">
                <div class="title">
                  <div class="container">
                    <div class="row"> 
                      <div class="col-lg-12">
                        <h1 class="title-line">Aprende con nosotros</h1>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="container">
                  <div class="row">
                    <div class="col-lg-6">
                      <div data-equalize="box-content-clinic" class="box">
                        <div class="content-picture get-image">
                          <div class="image"><img src="{{ get_field('left_box_img')['url'] }}" alt="Clínica de manejo"></div>
                        </div>
                        <div class="content-text"> 
                          <div class="inner"> 
                            <div class="box">
                              <div class="title"> 
                                <h2>{{ get_field('left_box_title') }}</h2>
                              </div>
                              <div class="excerpt">
                                {{ get_field('left_box_text') }}
                              </div>
                              @if (get_field('left_box_botton'))
                              	<div class="buttons"> <a href="{{ get_field('left_box_botton') }}" class="button white">Ver más</a></div>
                              @endif 
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div data-equalize="box-content-clinic" class="box">
                        <div class="content-picture get-image">
                          <div class="image"><img src="{{ get_field('right_box_img')['url'] }}" alt="Clínica de manejo"></div>
                        </div>
                        <div class="content-text"> 
                          <div class="inner"> 
                            <div class="box">
                              <div class="title"> 
                                <h2>{{ get_field('right_box_title') }}</h2>
                              </div>
                              <div class="excerpt">
                                {{ get_field('right_box_text') }}
                              </div>
                              @if (get_field('right_box_botton'))
                              	<div class="buttons"> <a href="{{ get_field('right_box_boton') }}" class="button white">Ver más</a></div>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
		</div>
	</div>
</div>

@overwrite