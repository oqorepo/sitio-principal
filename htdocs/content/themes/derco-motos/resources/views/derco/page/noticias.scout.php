@extends('derco.layouts.main')

@section('main')

	<?php

	global $post;

	if(!isset($_GET['marca'])):
		$sites = [ 1, 2, 3, 4 ];

	else:
		if($_GET['marca'] == 'suzuki')
			$sites = [2];

		if($_GET['marca'] == 'zoghshen')
			$sites = [3];

		if($_GET['marca'] == 'kymco')
			$sites = [4];

	endif;

	?>

	<div id="page">
		<div class="page-content">
			<div class="archive-news">
				@include('derco.section.header-title')
				<section class="news-archive">
					<div class="content">
						<div class="inner">
							<div class="limit">
								<div class="categories">

									<ul>
										<li class="{{count($sites) > 2 ? 'active' : ''}}">
											<a href="{{get_permalink(get_the_ID())}}" class="button red">Todas</a>
										</li>
										<li class="{{$sites[0] == 2 ? 'active' : ''}}">
											<a href="{{get_permalink(get_the_ID())}}?marca=suzuki" class="button red">Suzuki</a>
										</li>
										<li class="{{$sites[0] == 3 ? 'active' : ''}}">
											<a href="{{get_permalink(get_the_ID())}}?marca=zoghshen" class="button red">Zongshen</a>
										</li>
										<li class="{{$sites[0] == 4 ? 'active' : ''}}">
											<a href="{{get_permalink(get_the_ID())}}?marca=kymco" class="button red">Kymco</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="home-news">
					<div class="background"></div>
					<div class="content">
								<div class="items">
									<div class="container">
                    				<div class="row">
									<?php

										$query = new WP_Query_Multisite([
											'post_type' => 'post',
											'posts_per_page' => -1,
											'sites' => [
													'sites__in' => $sites
											]
										]);


										$i = 0;
										$page = empty(get_query_var('page')) ? 1 : get_query_var('page');

										while( $query->have_posts() ) : $query->the_post();

									?>
									<div class="col-lg-4 col-sm-6">
										<article class="item animate">
											<div class="picture get-image">
												<?php $params = ['width' => 280, 'height' => 330]; ?>
											 	<a href="noticias-single.html" class="image">
											 		<img src="{{bfiThumb::always(get_field('imagen_compartir')['url'], $params)}}" alt="{{get_field('imagen_compartir')['alt']}}">
											 	</a>
											</div>
											<div class="info">
												<div class="inner">
													<div class="date"><i class="fa fa-clock-o"></i>{{{get_the_date()}}}</div>
													<h2 data-equalize="home-news-item-item">{{{get_the_title()}}}</h2>
													<div class="excerpt">
														<p>{{get_field('bajada')}}</p>
													</div>
													<div class="buttons">
														<a href="{{get_the_permalink()}}"><i class="fa fa-plus"></i>Ver más</a>
													</div>
												</div>
											</div>
										</article>
									</div>
									<?php $i++; endwhile; wp_reset_postdata(); ?>
									</div>
									</div>
						</div>
					</div>
				</section>


				<section class="pagination">
					<div class="content">
						<div class="inner">
							<div class="limit">
							<!--
								<nav class="pagination">

									@if($page != 1)
										<a href="{{ get_permalink( $post->ID ) . ( $page - 1 ) }}" class="prev page-numbers">« Anterior</a>
									@endif

									<a href="{{ get_permalink( $post->ID ) . ( $page + 1 ) }}"  class="next page-numbers">Siguiente »</a></nav>

									 -->
							</div>
						</div>
					</div>
				</section>

			</div>
		</div>
	</div>

@stop