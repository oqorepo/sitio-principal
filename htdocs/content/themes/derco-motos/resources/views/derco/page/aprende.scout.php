@extends('derco.layouts.main')

@section('main')

    <div id="page">
        <div class="page-content">
            <div class="page-home">
                <!-- @include('derco.section.header-title') -->
                
            <section class="home-clinic">
              <div class="content">
                <div class="title">
                  <div class="container">
                    <div class="row"> 
                      <div class="col-lg-12">
                        <h1 class="title-line">Aprende con nosotros</h1>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="container">
                  <div class="row">
                    <div class="col-lg-6">
                      <div data-equalize="box-content-clinic" class="box">
                        <div class="content-picture get-image">
                          <div class="image"><img src="{{ get_field('left_box_img', '4' )['url'] }}" alt="Clínica de manejo"></div>
                        </div>
                        <div class="content-text"> 
                          <div class="inner"> 
                            <div class="box">
                              <div class="title"> 
                                <h2>{{ get_field('left_box_title', 4) }}</h2>
                              </div>
                              <div class="excerpt">
                                {{ get_field('left_box_text', 4) }}
                              </div>
                              @if (get_field('left_box_botton', 4))
                                <div class="buttons"> <a href="{{ get_field('left_box_botton', 4) }}" class="button white">Ver más</a></div>
                              @endif 
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div data-equalize="box-content-clinic" class="box">
                        <div class="content-picture get-image">
                          <div class="image"><img src="{{ get_field('right_box_img', 4)['url'] }}" alt="Clínica de manejo"></div>
                        </div>
                        <div class="content-text"> 
                          <div class="inner"> 
                            <div class="box">
                              <div class="title"> 
                                <h2>{{ get_field('right_box_title', 4) }}</h2>
                              </div>
                              <div class="excerpt">
                                {{ get_field('right_box_text', 4) }}
                              </div>
                              @if (get_field('right_box_botton', 4))
                                <div class="buttons"> <a href="{{ get_field('right_box_boton', 4) }}" class="button white">Ver más</a></div>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
                
            </div>
        </div>
    </div>

@overwrite