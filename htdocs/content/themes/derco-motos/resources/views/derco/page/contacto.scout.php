@extends('derco.layouts.main')

@section('main')

	<div id="page">
		<div class="page-content">
			<div class="single-news">

				@include('derco.section.header-title')
				<section class="contact">
					<div class="content">
						<div class="inner">
							<div class="limit">
								<div class="left">
									<div class="title">
										<h2>{{{get_field('titulo_formulario')}}}</h2>
										<div class="excerpt">
											{{get_field('descripcion_formulario')}}
										</div>
									</div>
									<form action="{{helpers::ajax_url()}}" method="post">
										<div class="fields">
											<div class="field">
												<div class="control firstname text">
													<label for="firstname" class="control-label">Nombres</label>
													<input class="control-input" type="text" id="firstname_6021" name="firstname" required="required" data-msg="Ingresa tu nombre"/>
												</div>
											</div>
											<div class="field">
												<div class="control lastname text">
													<label for="lastname" class="control-label">Apellidos</label>
													<input class="control-input" type="text" id="lastname_67053" name="lastname" required="required" data-msg="Ingresa tu apellido"/>
												</div>
											</div>
											<div class="field">
												<div class="control rut rut">
													<label for="rut" class="control-label">Rut</label>
													<input class="control-input rut" type="text" id="rut_71259" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido"/>
												</div>
											</div>
											<div class="field">
												<div class="control email email">
													<label for="email" class="control-label">E-mail</label>
													<input class="control-input" type="email" id="email_37522" name="email" required="required" data-msg="Ingresa un correo electrónico válido"/>
												</div>
											</div>
											<div class="field">
												<div class="control phone phone">
													<label for="phone" class="control-label">Teléfono</label>
													<input class="control-input" type="phone" id="phone_57278" name="phone" required="required" maxlength="9" placeholder="9 digitos" data-msg="Ingresa un teléfono de contacto"/>
												</div>
											</div>
											<div class="field">
												<div class="control message textarea">
													<label for="message" class="control-label">Mensaje</label>
													<textarea class="control-textarea" type="text" id="message_40639" name="message" placeholder="" required="required" data-msg="Escríbanos un mensaje"></textarea>
												</div>
											</div>
										</div>
										<div class="fields errors">
											<div class="control-error">
												<div class="errors">
													<div class="inner">
														<h4>Por favor corrige los siguiente errores:</h4>
														<ul> </ul>
													</div>
												</div>
											</div>
										</div>
										<div class="fields">
											<div class="control">
												<input type="submit" value="Enviar" role="form" class="control-submit"/>
											</div>
										</div>
										<input type="hidden" name="action" value="contacto">
										<input type="hidden" name="send_text" value="{{{get_field('texto_para_mensaje_enviado', 'option')}}}">
									</form>
								</div>
								<div class="right">
									<div class="banner">
										<div class="banner-background get-image">
											<div class="image"> <img src="{{bfiThumb::always(get_field('imagen_de_fondo_derco_responde'), ['width' => 440, 'height' => 590, 'opacity' => 50, 'grayscale' => true] )}}"></div>
										</div>
										<div class="banner-content">
											<div class="inner">
												<h2 class="title-line white">{{{get_field('titulo_responde')}}}</h2>
												<p>{{get_field('bajada_responde')}}</p>
												<div class="phone">
													<a href="tel:{{{get_field('telefono', 'option')}}}">
														<i class="fa fa-phone"></i> {{{get_field('telefono', 'option')}}}
													</a>
												</div>
												<p>{{get_field('horario', 'option')}}</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="map">
					<div class="googlemap">
						<div data-lat="{{get_field('ubicacion_mapa')['lat']}}" data-lng="{{get_field('ubicacion_mapa')['lng']}}" data-pin="{{themosis_assets()}}/img/pin_red.png" data-show="true" data-pin-width='38' data-pin-height='48' class="marker">
							<h4>{{get_bloginfo('name')}}</h4>
							<p class="address">{{{get_field('ubicacion_mapa')['address']}}}</p>
							<p>{{get_field('telefono', 'option')}}</p>
							<p>
								<a href="{{get_field('url_google_maps', 'option')}}" target="_blank"> <i class="fa fa-map-marker"> </i>Ver en Google Maps</a>
							</p>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

@stop
