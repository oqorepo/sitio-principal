@extends('derco.layouts.main')

@section('main')

	<div id="page">
		<div class="page-content">
			<div class="single-news">

				@include('derco.section.header-title',  ['disable_content' => true])

				<section class="news-single">
					<div class="content">
						<div class="inner">
							<div class="limit">
								<div class="box">
									<div class="inner">
										<article>
											<div class="paragraph">
												{{Loop::content()}}
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
		</div>
	</div>

@stop