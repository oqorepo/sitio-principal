@extends('derco.layouts.main')

@section('main')

	<div id="page">
		<div class="page-content">
			<div class="archive-news">
				@include('derco.section.header-title')
				<section class="contact">
					<div class="content">
						<div class="inner">
							<div class="limit">
								<div class="left">
									<div class="title">
										<h2>{{get_field('titulo_formulario')}}</h2>
										<div class="excerpt">falta texto !!</div>
									</div>
									<form action="../assets/json/success.json">
										<div class="fields">
											<div class="field">
												<div class="control firstname text">
													<label for="firstname" class="control-label">Nombres</label>
													<input class="control-input" type="text" id="firstname_6021" name="firstname" required="required" data-msg="Ingresa tu nombre"/>
												</div>
											</div>
											<div class="field">
												<div class="control lastname text">
													<label for="lastname" class="control-label">Apellidos</label>
													<input class="control-input" type="text" id="lastname_67053" name="lastname" required="required" data-msg="Ingresa tu apellido"/>
												</div>
											</div>
											<div class="field">
												<div class="control rut rut">
													<label for="rut" class="control-label">Rut</label>
													<input class="control-input rut" type="text" id="rut_71259" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido"/>
												</div>
											</div>
											<div class="field">
												<div class="control email email">
													<label for="email" class="control-label">E-mail</label>
													<input class="control-input" type="email" id="email_37522" name="email" required="required" data-msg="Ingresa un correo electrónico válido"/>
												</div>
											</div>
											<div class="field">
												<div class="control phone phone">
													<label for="phone" class="control-label">Teléfono</label>
													<input class="control-input" type="phone" id="phone_57278" name="phone" required="required" data-msg="Ingresa un teléfono de contacto"/>
												</div>
											</div>
											<div class="field">
												<div class="control message textarea">
													<label for="message" class="control-label">Mensaje</label>
													<textarea class="control-textarea" type="text" id="message_40639" name="message" placeholder="" required="required" data-msg="Escríbanos un mensaje"></textarea>
												</div>
											</div>
										</div>
										<div class="fields errors">
											<div class="control-error">
												<div class="errors">
													<div class="inner">
														<h4>Por favor corrige los siguiente errores:</h4>
														<ul> </ul>
													</div>
												</div>
											</div>
										</div>
										<div class="fields">
											<div class="control">
												<input type="submit" value="Enviar" role="form" class="control-submit"/>
											</div>
										</div>
									</form>
								</div>
								<div class="right">
									<div class="banner">
										<div class="banner-background get-image">
											<div class="image"> <img src="../assets/img/contact-banner.jpg" alt=""></div>
										</div>
										<div class="banner-content">
											<div class="inner">
												<h2 class="title-line white">{{get_field('titulo_responde')}}</h2>
												<p>{{get_field('bajada_responde')}}</p>
												<div class="phone"> <a href="tel:{{get_field('telefono', 'option')}}"><i class="fa fa-phone"> </i>{{get_field('telefono', 'option')}}</a></div>
												<p>{{get_field('horario', 'option')}}</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<?php $map = get_field('ubicacion_mapa'); ?>
				<section class="map">
					<div class="googlemap">
						<div data-lat="{{$map['lat']}}" data-lng="{{$map['lng']}}" data-pin="../assets/img/pin_red.png" data-pin-width='38' data-pin-height='48' class="marker">
							<h4>Digital Meat</h4>
							<p class="address">{{{$map['address']}}}</p>
							<p>{{get_field('telefono', 'option')}}</p>
							<p> <a href="#" target="_blank"> <i class="fa fa-map-marker"> </i>Ver en Google Maps</a></p>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

	<!--


	{{Form::open('', 'post', false, [
		'nonce'         => 'action',
		'nonce_action'  => 'edit_something'
	])}}

	{{ Form::label('Nombre',['for'=>'nombre']) }}
	{{ Form::text('nombre')}}

	{{ Form::label('Asunto',['for'=>'asunto']) }}
	{{ Form::text('asunto') }}

	{{ Form::label('Rut',['for'=>'rut']) }}
	{{ Form::text('rut') }}

	{{ Form::label('Comenntario',['for'=>'comentario']) }}
	{{ Form::textarea('comentario') }}

	{{ Form::label('E-mail',['for'=>'email']) }}
	{{ Form::email('email')}}

	{{ Form::label('Teléfono',['for'=>'telefono']) }}
	{{ Form::text('telefono', '+56 9 ')}}

	{{ Form::submit('send', 'Enviar datos') }}

	{{ Form::close() }}

-->

@overwrite