@extends('derco.layouts.main')

@section('main')

    <div id="page">
        <div class="page-content">
            <div class="page-home">
                @include('derco.section.header-title')
                <section class="clinic-introduce">
                    <div class="content">
                        <div class="inner">
                            <div class="limit">
                                <div class="box">
                                    <div class="border">
                                        <div data-equalize="clinic-introduce" class="col info">
                                            <article>
                                                <div class="inner">
                                                    <div class="title">
                                                        <h2 class="title-line">{{get_field('titulo_introduccion_clinica')}}</h2>
                                                    </div>
                                                    <div class="paragraph">
                                                        {{get_field('descripcion_introduccion_clinica')}}
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div data-equalize="clinic-introduce" class="col picture get-image">
                                            <div class="image">
                                                <img src="{{get_field('imagen_destacada_introduccion_clinica')['url']}}" alt="{{get_field('imagen_destacada_introduccion_clinica')['alt']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @if ( get_field('titulo_video_clinica') )
                    <section class="clinic-video">
                        <div class="content">
                            <div class="content-picture get-image">
                                <div class="image">
                                    <img src="{{bfiThumb::always(get_field('background_video_clinica')['url'], ['width' => 580, 'height' => 870])}}" alt="{{get_field('background_video_clinica')['alt']}}">
                                </div>
                            </div>
                            <div class="content-text">
                                <div class="inner">
                                    <div class="box">
                                        <div class="title">
                                            <h2 class="title-line white">{{get_field('titulo_video_clinica')}}</h2>
                                        </div>
                                        <div class="excerpt">
                                            {{get_field('descripcion_video_clinica')}}
                                        </div>
                                        <div class="buttons">
                                            <a href="http://www.youtube.com/watch?v={{get_field('id_youtube')}}" class="youtube-video button white">Ver video<i class="fa fa-caret-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
                <section class="clinic-gallery">
                    <div class="content">
                        <div class="inner">
                            <div class="limit">
                                <div class="title">
                                    <h2 class="title-line white">Galería de imágenes</h2>
                                </div>
                                <div class="items">

                                    <?php $i = 5; ?>
                                    @foreach(get_field('galeria_clinica') as $img)

                                        <article class="item {{ ($i % 5 == 0) ? 'middle' : ''}}">
                                            <div class="inner">
                                                <div class="picture get-image">
                                                    <a href="{{$img['url']}}" rel="clinic-gallery" class="fancybox image">
                                                        <img src="{{bfiThumb::always($img['url'], ['width' => 440, 'height' => 200] )}}" alt="{{$img['alt']}}">
                                                    </a>
                                                </div>
                                            </div>
                                        </article>

                                        <?php
                                            if($i == 10) { $i = 4; }
                                            $i++;
                                        ?>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="clinic-contact">
                    <div class="content">
                        <div class="inner">
                            <div class="limit">
                                <div class="logo"> <img src="//www.dercomotos.cl/content/uploads/2017/10/Logo-Escuela-e1507667517744.png" alt="Clínica de conducción"></div>
                                <div class="title">
                                    <h2 class="title-line">{{get_field('titulo_contacto_clinica')}}</h2>
                                    <div class="excerpt">{{get_field('descripcion_formulario_contacto_clinica')}}</div>
                                </div>
                                <form action="{{helpers::ajax_url()}}" method="post">
                                    <div class="fields">
                                        <div class="field">
                                            <div class="control firstname text">
                                                <label for="fullname" class="control-label">Nombre</label>
                                                <input class="control-input" type="text" id="firstname_34565" name="fullname" required="required" data-msg="Ingresa tu nombre"/>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="control rut rut">
                                                <label for="rut" class="control-label">Rut</label>
                                                <input class="control-input rut" type="text" id="rut_28445" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido"/>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="control email email">
                                                <label for="email" class="control-label">E-mail</label>
                                                <input class="control-input" type="email" id="email_78296" name="email" required="required" data-msg="Ingresa un correo electrónico válido"/>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="control phone phone">
                                                <label for="phone" class="control-label">Teléfono</label>
                                                <input class="control-input" type="phone" id="phone_82873" name="phone" maxlength="9" required="required" data-msg="Ingresa un teléfono de contacto"/>
                                            </div>
                                        </div>
                                        @if (get_field('clinica_schedule'))
                                            <div class="field">
                                                <div class="control phone phone">
                                                    <label for="schedule" class="control-label">Horario</label>
                                                    <select class="control-input" name="schedule" id="shedule_82873" required="required" data-msg="Ingresa un horario">
                                                        <option value="" default>Selecciona horario</option>
                                                        <?php foreach (get_field('clinica_schedule') as $schedule): ?>
                                                            <option value="{{ $schedule['horario'] }}">{{ $schedule['horario'] }}</option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="fields errors">
                                        <div class="control-error">
                                            <div class="errors">
                                                <div class="inner">
                                                    <h4>Por favor corrige los siguiente errores:</h4>
                                                    <ul> </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fields submit">
                                        <div class="control">
                                            <input type="submit" value="Enviar" role="form" class="control-submit"/>
                                        </div>
                                    </div>
                                    <input type="hidden" name="action" value="clinica">
                                    <input type="hidden" name="send_text" value="{{{get_field('texto_para_mensaje_enviado_clinica', 'option')}}}">
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@overwrite