@extends('derco.layouts.main')

@section('main')

	<div id="page">
		<div class="page-content">
			<div class="single-news">

				<section class="page-title">
					<div class="background picture get-image">
						<div class="image">
							@if(get_field('imagen_cabecera')['url'])
								<img src="{{get_field('imagen_cabecera')['url']}}" >
							@else
								<img src="{{get_field('imagen_cabecera_titulo', 'option')}}" >
							@endif
						</div>
					</div>
					<div class="content">
						<div class="inner">
							<div class="limit">
								<div class="title">
									<h1 class="title-line white">404</h1>
								</div>
							</div>
						</div>
					</div>
				</section>


				<section class="news-single">
					<div class="content">
						<div class="inner">
							<div class="limit">
								<div class="box">
									<div class="inner">
										<article>
											<div class="paragraph">
												<p style="text-align: center;">Página no encontrada.</p>
												<p style="text-align: center;">
													<a href="{{get_bloginfo('url')}}" title="Volver al sitio">Volver al sitio</a>
												</p>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
		</div>
	</div>

@stop