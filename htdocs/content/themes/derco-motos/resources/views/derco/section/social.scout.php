<div class="social">
	<nav>
		<ul>
			@if(get_field('redes_sociales', 'option'))
				@while(has_sub_field('redes_sociales', 'option'))
					<li><a href="{{get_sub_field('url', 'option')}}" target="_blank" title="{{get_sub_field('titulo', 'option')}}">{{get_sub_field('icono', 'option')}} </a></li>
				@endwhile
			@endif
		</ul>
	</nav>
</div>