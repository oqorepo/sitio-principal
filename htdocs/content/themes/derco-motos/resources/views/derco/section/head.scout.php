<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M38DMHM');</script>
	<!-- End Google Tag Manager -->
	
	<title><?php wp_title() ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">

	<!-- Favicon-->
	<link rel="apple-touch-icon" sizes="57x57" href="{{themosis_assets()}}/favicon-derco/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="{{themosis_assets()}}/favicon-derco/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="{{themosis_assets()}}/favicon-derco/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="{{themosis_assets()}}/favicon-derco/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="{{themosis_assets()}}/favicon-derco/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="{{themosis_assets()}}/favicon-derco/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="{{themosis_assets()}}/favicon-derco/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="{{themosis_assets()}}/favicon-derco/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="{{themosis_assets()}}/favicon-derco/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="{{themosis_assets()}}/favicon-derco/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="{{themosis_assets()}}/favicon-derco/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="{{themosis_assets()}}/favicon-derco/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="{{themosis_assets()}}/favicon-derco/favicon-16x16.png">
	<link rel="manifest" href="{{themosis_assets()}}/favicon-derco/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{themosis_assets()}}/favicon-derco/ms-icon-144x144.png">
	<meta name="theme-color" content="#ed1a2d">

	<?php wp_head() ?>

	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->


</head>