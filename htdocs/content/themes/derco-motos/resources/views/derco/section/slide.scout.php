@if(get_field('slide'))
	<section class="home-introduce">
		<div class="background"></div>
		<div class="content">
			<ul class="slider slick">

				@while(has_sub_field('slide'))

					<li class="slider-item">
						<div class="slider-background get-image">
							<div class="image">
								<img src="{{bfiThumb::always(get_sub_field('imagen')['url'], ['width' => 1440, 'height' => 770])}}" alt="{{get_sub_field('imagen')['alt']}}">
							</div>
						</div>
						<div class="slider-content">
							<div class="box">
								<div class="inner">
									<div class="box-content">
										<h2 class="title-line white">{{{get_sub_field('titulo')}}}</h2>
										<div class="excerpt">{{get_sub_field('descripcion')}}</div>

										@if(!empty(get_sub_field('boton')[0]))
											<?php $target = get_sub_field('abrir_en_nueva_ventana'); ?>
											<div class="buttons" target="{{{isset($target)?'_blank':''}}} ">
												<a href="{{get_sub_field('url_boton')}}" class="button red">{{{get_sub_field('texto_boton')}}}</a>
											</div>
										@endif

									</div>
								</div>
							</div>
						</div>
					</li>

				@endwhile

			</ul>
		</div>
	</section>
@endif