@loop

	<section class="page-title">
		<div class="background picture get-image">
			<div class="image">
				<div class="image">
					@if(get_field('imagen_cabecera')['url'])
						<img src="{{bfiThumb::always(get_field('imagen_cabecera')['url'], ['width' => 1440, 'height' => 430])}}" alt="{{get_field('imagen_cabecera')['alt']}}">
					@else
						<img src="{{get_field('imagen_cabecera_titulo', 'option')}}" >
					@endif
				</div>
			</div>
		</div>
		<div class="content">
			<div class="inner">
				<div class="limit">
					<?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb('<div class="breadcrumb">','</div>'); ?>
					<div class="title">
						<h1 class="title-line white">{{ Loop::title() }}</h1>
						<div class="excerpt">
							@if(empty($disable_content))
								<div class="excerpt">
									<p>{{ Loop::content() }}</p>
								</div>
							@endif
						</div>
						<?php if (isset($button)): ?>
							<div class="buttons"><a href="{{ get_permalink(1076) }}" class="button white">Agenda tu hora</a></div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</section>

@endloop
