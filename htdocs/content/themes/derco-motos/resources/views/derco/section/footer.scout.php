<footer>
    <div class="footer">
        <section class="top">
            <div class="inner">
                <div class="limit">
                    <div class="logo"> <img src="{{get_field('logo_footer','option')['url']}}" alt="Derco Motos"></div>
                </div>
            </div>
        </section>
        <section class="middle">
            <div class="inner">
                <div class="limit">
                    <div class="col phone">
                        <div class="box icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="box number">
                            {{get_field('titulo_telefono', 'option')}}:
                            <a href="tel:{{get_field('telefono', 'option')}}">{{get_field('telefono', 'option')}}</a>
                        </div>
                        <div class="box text"> <span>{{get_field('horario', 'option')}}</span></div>
                    </div>
                    <div class="col address">
                        <div class="box"> <i class="fa fa-map-marker"> </i>{{get_field('direccion', 'option')}}
                        </div>
                    </div>
                    <div class="col buttons">
                        <a href="{{get_field('pagina_de_contacto', 'option')->guid}}" class="button white">{{get_field('boton_de_contacto_texto', 'option')}}</a>
                        @if(get_field('redes_sociales', 'option'))
                            @while(has_sub_field('redes_sociales', 'option'))
                                <a href="{{get_sub_field('url', 'option')}}" target="_blank" title="{{get_sub_field('titulo', 'option')}}" class="social facebook">{{get_sub_field('icono', 'option')}}</a>
                            @endwhile
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <section class="links">
            <div class="inner">
                <div class="limit"> 
                    <ul>
                        @if (get_field('links_footer_items', 'option'))
                            @while (has_sub_field('links_footer_items', 'option'))
                                @if (get_sub_field('icon_type', 'option') == 'font')
                                    <li>
                                        <a href="#">
                                            <span class="icon"><i class="fa {{ get_sub_field('font_awesome', 'option') }}"></i></span><span>{{ get_sub_field('text', 'option') }}</span>
                                        </a>
                                    </li>
                                @elseif (get_sub_field('icon_type', 'option') == 'img')
                                    <li>
                                        <a href="{{ get_sub_field('link', 'option') }}">
                                        <span class="icon"><img src="{{ get_sub_field('icon', 'option')['url'] }}" alt="{{ get_sub_field('icon', 'option') ? 'Derco Motos' : '' }}"></span><span>{{ get_sub_field('text', 'option') }}</span>
                                        </a>
                                    </li>
                                @endif
                            @endwhile
                        @endif
                    </ul>
                </div>
            </div>
        </section>
        <section class="bottom">
            <div class="inner">
                <div class="limit">
                    <p>{{get_field('derechos_reservados', 'option')}}</p>
                </div>
            </div>
        </section>
    </div>
</footer>

<?php wp_footer() ?>
