<section class="page-title">
    <div class="background picture get-image">
        <div class="image"> <img src="{{ $img }}" alt=""></div>
    </div>
    <div class="content">
        <div class="inner">
            <div class="limit">
                <div class="breadcrumb"><a href="{{ get_permalink(1060) }}">Inicio / </a><span>Clínica de manejo</span></div>
                <div class="title">
                    <h1 class="title-line title-clinic white">Clínica de manejo</h1>
                </div>
                <div class="bullets">
                    <ul>
                        <li><a href="#" class="{{ $step == 1 ? 'is-active' : '' }}"><span class="number">1</span><span class="text">Datos</span></a></li>
                        <li><a href="#" class="{{ $step == 2 ? 'is-active' : '' }}"><span class="number">2</span><span class="text">Agenda</span></a></li>
                        <li><a href="#" class="{{ $step == 3 ? 'is-active' : '' }}"><span class="number">3</span><span class="text">Verificación</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>