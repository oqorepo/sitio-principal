@loop
@extends('derco.layouts.clinica')

@section('content')

    @include('derco.clinica.head', [
        'step'  => 3,
        'img'   => get_field('imagen_cabecera', Loop::ID())['url']
    ])
    <section class="clinic-form">
        <div class="content">
            <div class="inner">
                <div class="limit">
                    <form class="form-clinic">
                        <div class="form-text">
                            {{ Loop::content() }}
                        </div>
                        @if (isset($_GET['calendar']) && $_GET['calendar'] == 'true')
                            <div class="form-success">
                                <p>Evento agregado al calendario exitosamente.</p>
                            </div>
                        @elseif ((isset($_GET['calendar']) && !$_GET['calendar'] == 'false')) 
                            <div class="form-error">
                                <p>No se pudo agregar el evento a su calendario.</p>
                            </div>
                        @else

                        @endif
                        <div class="table-fields">
                            <h1 class="table-name"><i class="fa fa-calendar"> </i><span>Verificación</span></h1>
                            <div class="table-row">
                                <div class="row-details">
                                    <div class="details-content"><span>{{ $_SESSION['clinic']['firstname'].' '.$_SESSION['clinic']['lastname'] }}</span>
                                        <div class="details-edit"><a href="/clinica-de-manejo/?edit=true&id=<?php echo $_SESSION['clinic']['id']; ?>"> <span>Editar </span><i class="fa fa-cog"></i></a></div>
                                    </div>
                                </div>
                                <div class="row-details">
                                    <div class="details-content"><i class="fa fa-calendar"></i><span>{{ $_SESSION['clinic']['date'] }}</span><i class="fa fa-clock-o"></i><span>{{ $_SESSION['clinic']['time']}}</span>
                                        <div class="details-edit"><a href="/clinica-de-manejo/paso-2?edit=true&id=<?php echo $_SESSION['clinic']['id']; ?>"> <span>Editar </span><i class="fa fa-cog"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-submit">
                                <div class="control"> 
                                    <?php 
                                    $login_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri='.urlencode('https://www.dercomotos.cl/googleCalendar/callback.php').'&response_type=code&client_id=324923200946-i101913suvgeamhcg56kvsi6ld2eqc6m.apps.googleusercontent.com&access_type=online';
                                    ?>

                                    <div class="table-submit">
                                        <a href="<?php echo $login_url; ?>" class="button red"><span>Guardar en calendario</span></a>
                                        <a href="{{ home_url() }}" class="button red"><span>Salir</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop
@endloop