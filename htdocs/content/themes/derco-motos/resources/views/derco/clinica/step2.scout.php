@loop
@extends('derco.layouts.clinica')

@section('content')

    @include('derco.clinica.head', [
        'step' => 2,
        'img'   => get_field('imagen_cabecera', Loop::ID())['url']
    ])
    <section class="clinic-form">
            <div class="content">
                <div class="inner">
                    <div class="limit">
                        <form action="{{ helpers::ajax_url() }}" class="form-clinic" method="POST">
                            <div class="form-text">
                                {{ Loop::content() }}
                            </div>
                            <div class="table-fields">
                                <h1 class="table-name"><i class="fa fa-calendar"> </i><span>{{ get_field('clinic_text_date') }}</span></h1>
                                <div class="table-row">
                                    <div class="row-title"> <i class="fa fa-clock-o"></i><span>Horarios</span></div>
                                    <div class="row-title"><i class="fa fa-user"></i><span>Cupos disponibles</span></div>
                                </div>
                                @foreach (get_field('clinic_shedule') as $key => $schedule)
                                    <div class="table-row">
                                        <div class="row-select">
                                            <span>
                                                @if ($schedule['used'] > 0)
                                                    <input type="radio" name="time" id="time_1" value="{{ $key }}.{{ $schedule['hour'] }}">
                                                @endif
                                            </span>
                                            <span {{ ($schedule['used'] > 0) ? 'style="padding-left: 17px;"' : '' }}>{{ $schedule['hour'] }}</span>
                                        </div>
                                        <div class="row-select"><span> 
                                            @if ($schedule['used'] == 0)
                                                <span>Sin cupos</span>
                                            @else
                                                <label for="time_1">{{ $schedule['used'] }} / {{ $schedule['quotas'] }}</label></span>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                                <input type="hidden" value="{{ get_field('clinic_date') }}" name="date">
                                <input type="hidden" value="{{ Loop::ID() }}" name="id">
                                <input type="hidden" value="step2" name="action">
                                <div class="table-submit">
                                    <div class="control">
                                        <input type="submit" value="Confirmar asistencia" role="form" class="control-submit" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
@stop
@endloop