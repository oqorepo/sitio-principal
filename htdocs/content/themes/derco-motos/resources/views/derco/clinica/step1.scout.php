@loop
@extends('derco.layouts.clinica')

@section('content')

    @include('derco.clinica.head', [
        'step'  => 1,
        'img'   => get_field('imagen_cabecera', Loop::ID())['url']
    ])
<?php 
    if (isset($_GET['edit']) && isset($_SESSION['clinic'])) {
            $action = 'edituser';
        } else {
            $action = 'step1';
        }
?>
    <section class="clinic-form">
        <div class="content">
            <div class="inner">
                <div class="limit">
                    <form class="form-clinic" action="{{ helpers::ajax_url() }}" method="POST">
                        <input type="hidden" name="action" value="{{ $action }}">
                        @if (isset($_SESSION['clinic']['id']))
                            <input type="hidden" name="id" value="{{ $_SESSION['clinic']['id'] }}">
                        @endif

                        <div class="form-text">
                            {{ Loop::content() }}
                        </div>
                        @if (isset($_GET['delete']) && $_GET['delete'])
                            <div class="form-success">
                                <?php echo @$_SESSION['delete_success']; ?>
                            </div>
                        @endif
                        <div class="form-fields">
                            <div class="fields">
                                <div class="field">
                                    <div class="control firstname text">
                                        <label for="firstname" class="control-label">Nombres</label>
                                        <input class="control-input" type="text" id="firstname" name="firstname" required="required" data-msg="Ingresa tu nombre" value="{{ (isset($_GET['edit']) && isset($_SESSION['clinic'])) ? $_SESSION['clinic']['firstname'] : '' }}"/>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control lastname text">
                                        <label for="lastname" class="control-label">Apellidos</label>
                                        <input class="control-input" type="text" id="lastname" name="lastname" required="required" data-msg="Ingresa tu apellido" value="{{ (isset($_GET['edit']) && isset($_SESSION['clinic'])) ? $_SESSION['clinic']['lastname'] : '' }}"/>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control rut rut">
                                        <label for="rut" class="control-label">Rut</label>
                                        <input class="control-input rut" type="text" id="rut" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido" value="{{ (isset($_GET['edit']) && isset($_SESSION['clinic'])) ? $_SESSION['clinic']['rut'] : '' }}"/>
                                        <small>Ingresa tu rut sin puntos ni guiones</small>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control email email">
                                        <label for="email" class="control-label">E-mail</label>
                                        <input class="control-input" type="email" id="email" name="email" required="required" data-msg="Ingresa un correo electrónico válido" value="{{ (isset($_GET['edit']) && isset($_SESSION['clinic'])) ? $_SESSION['clinic']['email'] : '' }}"/>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control phone phone">
                                        <label for="phone" class="control-label">Teléfono</label>
                                        <input class="control-input" type="phone" id="phone" name="phone" required="required" data-msg="Ingresa un teléfono de contacto" value="{{ (isset($_GET['edit']) && isset($_SESSION['clinic'])) ? $_SESSION['clinic']['phone'] : '' }}"/>
                                        <small>Ingresa solo números y antepone el codigo de país y ciudad</small>
                                    </div>
                                </div>
                            </div>
                            <div class="fields errors">
                                <div class="control-error">
                                    <div class="errors">
                                        <div class="inner">
                                            <h4>Por favor corrige los siguiente errores:</h4>
                                            <ul> </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fields">
                                <div class="control">
                                    <input type="submit" value="Enviar" role="form" class="control-submit" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop
@endloop