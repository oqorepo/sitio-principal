@loop 
@extends('derco.layouts.clinica') 
@section('content') 
@include('derco.clinica.head', [ 
    'step' => 2,
    'img'   => get_field('imagen_cabecera', Loop::ID())['url'],
])

<section class="clinic-form">
    <div class="content">
        <div class="inner">
            <div class="limit">
                <form class="form-clinic" action="{{ helpers::ajax_url() }}" method="POST" data-reset="false">
                    <input type="hidden" value="{{ $_SESSION['edit']->id}}" name="id">
                    <input type="hidden" name="action" value="edituser">
                    <div class="form-text">
                        {{ Loop::content() }}
                    </div>
                    <?php if (isset($_SESSION['edit_success'])) : ?>
                        <div class="form-success">
                            <?php echo $_SESSION['edit_success']; ?>
                            <?php
                                unset($_SESSION['edit_success']);
                            ?>
                        </div>
                   <?php endif; ?>
                    <div class="table-fields">
                        <h1 class="table-name"><i class="fa fa-calendar"> </i><span>Datos de reserva</span></h1>
                        <div class="table-row">
                            <div class="row-edit">
                                <button type="button" class="edit-trigger"><span>Editar</span><i class="fa fa-cog"></i></button>
                                <div class="control firstname text">
                                    <label for="firstname" class="control-label">Nombres</label>
                                    <input class="control-input" type="text" id="firstname" name="firstname" required="required" data-msg="Ingresa tu nombre" value="{{ $_SESSION['edit']->name }}" />
                                </div>
                            </div>
                        </div>
                        <div class="table-row">
                            <div class="row-edit">
                                <button type="button" class="edit-trigger"><span>Editar</span><i class="fa fa-cog"></i></button>
                                <div class="control lastname text">
                                    <label for="lastname" class="control-label">Apellidos</label>
                                    <input class="control-input" type="text" id="lastname" name="lastname" required="required" data-msg="Ingresa tu apellido" value="{{ $_SESSION['edit']->lastname }}" />
                                </div>
                            </div>
                        </div>
                        <div class="table-row">
                            <div class="row-edit">
                                <button type="button" class="edit-trigger"><span>Editar</span><i class="fa fa-cog"></i></button>
                                <div class="control rut rut">
                                    <label for="rut" class="control-label">Rut</label>
                                    <input class="control-input rut" type="text" id="rut" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido" value="{{ $_SESSION['edit']->rut }}" />
                                </div>
                            </div>
                        </div>
                        <div class="table-row">
                            <div class="row-edit">
                                <button type="button" class="edit-trigger"><span>Editar</span><i class="fa fa-cog"></i></button>
                                <div class="control email email">
                                    <label for="email" class="control-label">E-mail</label>
                                    <input class="control-input" type="email" id="email" name="email" required="required" data-msg="Ingresa un correo electrónico válido" value="{{ $_SESSION['edit']->email }}" />
                                </div>
                            </div>
                        </div>
                        <div class="table-row">
                            <div class="row-edit">
                                <button type="button" class="edit-trigger"><span>Editar</span><i class="fa fa-cog"></i></button>
                                <div class="control phone phone">
                                    <label for="phone" class="control-label">Teléfono</label>
                                    <input class="control-input" type="phone" id="phone" name="phone" required="required" data-msg="Ingresa un teléfono de contacto" value="{{ $_SESSION['edit']->phone }}" />
                                </div>
                            </div>
                            <div class="table-submit">
                                <div class="control">
                                    <input type="submit" value="Modificar datos" role="form" class="control-submit" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-fields">
                        <h1 class="table-name"><i class="fa fa-calendar"> </i><span>Horario de reserva</span></h1>
                        <div class="table-row">
                            <div class="row-reservation"><i class="fa fa-calendar"></i><span>{{ $_SESSION['edit']->inscription_date }}</span><i class="fa fa-clock-o"></i><span>{{ $_SESSION['edit']->inscription_hour }}</span>
                                <button type="button" class="edit-trigger change-date-trigger"><span>Cambiar fecha</span><i class="fa fa-cog"></i></button>
                            </div>
                        </div>
                </form>
                <div class="table-row">
                    <form class="form-clinic" action="{{ helpers::ajax_url() }}" method="POST" data-reset="false">
                        <input type="hidden" value="{{ $_SESSION['edit']->id}}" name="id">
                        <input type="hidden" name="fields" value="{{ $_SESSION['edit']->date_field }}">
                        <input type="hidden" value="deleteclinic" name="action">
                        <div class="table-submit">
                            <div class="control">
                                <input type="submit" value="Eliminar reserva" role="form" class="control-submit" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('modal')
<div id="schedule-popup">
    <form class="form-clinic" action="{{ helpers::ajax_url() }}" method="POST">

    	<input type="hidden" value="{{ $_SESSION['edit']->id}}" name="id">
        <input type="hidden" name="action" value="editschedule">
        <input type="hidden" name="fields" value="{{ $_SESSION['edit']->date_field }}">

        <div class="schedule-close"><i class="fa fa-times"></i></div>
        <div class="table-fields">
            <div class="table-row">
                <div class="row-title"> <i class="fa fa-clock-o"></i><span>Horarios</span></div>
                <div class="row-title"><i class="fa fa-clock-o"></i><span>Cupos disponibles</span></div>
            </div>
             @foreach (get_field('clinic_shedule', 1084) as $key => $schedule)
                <div class="table-row">
                    <div class="row-select">
                        <span>
                            @if ($schedule['used'] > 0)
                                <input type="radio" name="time" id="time_1" value="{{ $key }}.{{ $schedule['hour'] }}">
                            @endif
                        </span>
                        <span {{ ($schedule['used'] > 0) ? 'style="padding-left: 17px;"' : '' }}>{{ $schedule['hour'] }}</span>
                    </div>
                    <div class="row-select"><span> 
                        @if ($schedule['used'] == 0)
                            <span>Sin cupos</span>
                        @else
                            <label for="time_1">{{ $schedule['used'] }} / {{ $schedule['quotas'] }}</label></span>
                        @endif
                    </div>
                </div>
            @endforeach
            <div class="table-submit">
                <div class="control">
                    <input type="submit" value="Confirmar asistencia" role="form" class="control-submit" />
                </div>
            </div>
        </div>
    </form>
</div>
@stop 
@endloop