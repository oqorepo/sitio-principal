<section class="clinic-gallery">
	<div class="content">
		<div class="inner">
			<div class="limit">
				<div class="title">
					<h2 class="title-line white">{{get_sub_field('titulo')}}</h2>
				</div>
				<div class="items">

					<?php $i = 3; ?>
					@foreach(get_sub_field('galeria_de_imagenes') as $img)

						<article class="item {{ ($i % 3 == 0) ? 'middle' : ''}}">
							<div class="inner">
								<div class="picture get-image"><a href="{{$img['url']}}" rel="clinic-gallery" class="fancybox image"> <img src="{{$img['url']}}" alt="{{$img['alt']}}"></a></div>
							</div>
						</article>

						<?php $i++ ?>
					@endforeach

				</div>
			</div>
		</div>
	</div>
</section>


