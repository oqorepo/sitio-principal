@extends('derco.layouts.main')

@section('main')

	@if(get_field('desea_redireccionar'))

		<?php

			header("HTTP/1.1 301 Moved Permanently");
			header("Location: " . get_field('pagina_para_redireccion')->guid);

		?>

	@endif

<div id="page">
	<div class="page-content">
		<div class="page-clinic">
			@include('derco.section.header-title')
		</div>
	</div>
</div>

@stop