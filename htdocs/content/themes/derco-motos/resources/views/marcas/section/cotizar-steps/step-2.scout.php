<?php

    $cities = [
            [
                    'name'   => 'primera',
                    'number' => 'I',
                    'title'  => 'región de tarapacá',
                    'id'     => 1
            ],
            [
                    'name'   => 'segunda',
                    'number' => 'II',
                    'title'  => 'región de antofagasta',
                    'id'     => 2
            ],
            [
                    'name'   => 'tercera',
                    'number' => 'III',
                    'title'  => 'región de atacama',
                    'id'     => 3
            ],
            [
                    'name'   => 'cuarta',
                    'number' => 'IV',
                    'title'  => 'región de coquimbo',
                    'id'     => 4
            ],
            [
                    'name'   => 'quinta',
                    'number' => 'V',
                    'title'  => 'región de valparaiso',
                    'id'     => 5
            ],
            [
                    'name'   => 'sexta',
                    'number' => 'VI',
                    'title'  => 'región del libertador general bernardo o\'higgins',
                    'id'     => 6
            ],
            [
                    'name'   => 'séptima',
                    'number' => 'VII',
                    'title'  => 'región del maule',
                    'id'     => 7
            ],
            [
                    'name'   => 'octava',
                    'number' => 'VIII',
                    'title'  => 'región del bío - bío',
                    'id'     => 8
            ],
            [
                    'name'   => 'novena',
                    'number' => 'IX',
                    'title'  => 'región de la araucanía',
                    'id'     => 9
            ],
            [
                    'name'   => 'décima',
                    'number' => 'X',
                    'title'  => 'región de los lagos',
                    'id'     => 10
            ],
            [
                    'name'   => 'decimoprimera',
                    'number' => 'XI',
                    'title'  => 'región aysén del general carlos ibáñez del campo',
                    'id'     => 11
            ],
            [
                    'name'   => 'decimosegunda',
                    'number' => 'XII',
                    'title'  => 'región de magallanes y la antártica chilena ',
                    'id'     => 12
            ],
            [
                    'name'   => 'metropolitana',
                    'number' => 'XIII',
                    'title'  => 'región metropolitana',
                    'id'     => 13
            ],
            [
                    'name'   => 'decimocuarta',
                    'number' => 'XIV',
                    'title'  => 'region de los ríos',
                    'id'     => 14
            ],
            [
                    'name'   => 'decimoquinta',
                    'number' => 'XV',
                    'title'  => 'región de arica y parinacota',
                    'id'     => 15
            ]
    ];


?>
<div class="step step-2">
    <div class="inner">
        <div class="limit">
            <form action="{{helpers::ajax_url()}}" method="post" class="custom">
                <div class="fields">
                    <div class="field">
                        <div class="field-2">
                            <div class="control firstname text">
                                <label for="firstname" class="control-label">Nombre</label>
                                <input class="control-input" type="text" id="firstname_70085" name="firstname" maxlength="100" required="required" data-msg="Ingresa tu nombre" value="{{isset($_SESSION['user']['firstname']) ? $_SESSION['user']['firstname'] : ''}}"/>
                            </div>
                        </div>
                        <div class="field-2">
                            <div class="control lastname text">
                                <label for="lastname" class="control-label">Apellidos</label>
                                <input class="control-input" type="text" id="lastname_8048" name="lastname" maxlength="100" required="required" data-msg="Ingresa tu apellido" value="{{isset($_SESSION['user']['lastname']) ? $_SESSION['user']['lastname'] : ''}}"/>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="field">
                        <div class="field-2 data-area">
                            <div data-json="{{helpers::ajax_url()}}?action=cities" data-trigger="comuna_personal" class="field-select">
                                <div class="control region_personal select">
                                    <label for="region_personal" class="control-label">Región</label>
                                    <select required class="control-select" id="region_personal_8272" name="region_personal" required="required" data-msg="Seleccione su región">
                                        @if(!empty($_SESSION['user']['region_personal']))
                                            <option value="{{$_SESSION['user']['region_personal']}}" selected="selected">{{ucwords($cities[($_SESSION['user']['region_personal'] -1)]['title'])}}</option>
                                        @else
                                            <option value="" disabled="disabled" selected="selected">Seleccione su región</option>
                                        @endif
                                        @foreach($cities as $city)
                                            <option value="{{$city['id']}}">{{ucwords($city['title'])}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field-2 data-location">
                            <div class="control comuna_personal select">
                                <label for="comuna_personal" class="control-label">Comuna</label>
                                <select required class="control-select" id="comuna_personal_56700" name="comuna_personal" required="required" data-msg="Seleccione su comuna">
                                    @if(isset($_SESSION['user']['comuna_personal']))
                                        <option value="{{$_SESSION['user']['comuna_personal']}}" selected="selected">{{$_SESSION['user']['comuna_personal']}}</option>
                                    @else
                                        <option value="" disabled="disabled" selected="selected">Seleccione su comuna</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div> -->
                    <div class="field">
                       <!--  <div class="field-2">
                            <div class="control address text">
                                <label for="address" class="control-label">Dirección</label>
                                <input class="control-input" type="text" id="address_37261" maxlength="50" name="address" data-msg="Ingresa tu dirección" value="{{isset($_SESSION['user']['address']) ? $_SESSION['user']['address'] : ''}}"/>
                            </div>
                        </div> -->
                        <div class="field-2">
                            <div class="control rut rut">
                                <label for="rut" class="control-label">Rut</label>
                                <input class="control-input rut" type="text" id="rut_39541" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido" value="{{isset($_SESSION['user']['rut']) ? $_SESSION['user']['rut'] : ''}}"/>
                            </div>
                        </div>
                        <div class="field-2">
                            <div class="control phone phone">
                                <label for="phone" class="control-label">Teléfono</label>
                                <input class="control-input" type="phone" minlength="9" maxlength="9" id="phone_79393" name="phone" placeholder="9 digitos" required="required" data-msg="Ingresa un teléfono de contacto de 9 digitos" value="{{isset($_SESSION['user']['phone']) ? $_SESSION['user']['phone'] : ''}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <!-- <div class="field-2"> -->
                            <div class="control email email">
                                <label for="email" class="control-label">E-mail</label>
                                <input class="control-input" type="email" id="email_89163" name="email" maxlength="160" required="required" data-msg="Ingresa un correo electrónico válido" value="{{isset($_SESSION['user']['email']) ? $_SESSION['user']['email'] : ''}}" />
                            </div>
                        <!-- </div> -->
                    </div>
                    <div class="field">
                        <h2>Selecciona un concesionario</h2>
                    </div>
                    <div class="field">
                        <div class="field-3 data-area">
                            <div data-json="{{helpers::ajax_url()}}?action=concesionarios&type=comuna" data-trigger="comuna_concessionaire" class="field-select">
                                <div class="control region_concessionaire select">
                                <label for="region_concessionaire" class="control-label">Región</label>
                                <select required class="control-select" id="region_concessionaire_59754" name="region_concessionaire" data-msg="Seleccione su región">
                                    <option value="" disabled="disabled" selected="selected">Seleccione su región</option>
                                    <?php
                                        $query = new WP_Query_Multisite( ['post_type' => 'page', 'sites' => [ 'sites__in' => [1] ] ] );
                                        while( $query->have_posts() ) : $query->the_post();
                                            if( get_post()->post_name  == 'concesionarios' ) :
                                    ?>
                                        @while(have_rows('region_zn')) <?php the_row(); ?>
                                            <option value="{{helpers::slugify(get_sub_field('nombre_region_zn'), '-')}}">{{{get_sub_field('nombre_region_zn')}}}</option>
                                        @endwhile
                                    <?php
                                            endif;
                                        endwhile;
                                        wp_reset_postdata();
                                    ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="field-3 data-location">
                            <div data-json="{{helpers::ajax_url()}}?action=concesionarios" data-trigger="concessionaire" class="field-select">
                                <div class="control comuna_concessionaire select">
                                    <label for="comuna_concessionaire" class="control-label">Comuna</label>
                                    <select required class="control-select" id="comuna_concessionaire_24470" name="comuna_concessionaire" data-msg="Seleccione su comuna" >
                                        <option value="" disabled="disabled" selected="selected">Seleccione su comuna</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field-3 data-concessionaire">
                            <div class="control concessionaire select">
                                <label for="concessionaire" class="control-label">Concesionario</label>
                                <select required class="control-select" id="concessionaire_76528" name="concessionaire" data-msg="Seleccione su comuna">
                                    <option value="" disabled="disabled" selected="selected">Seleccione su comuna</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fields errors">
                    <div class="control-error">
                        <div class="errors">
                            <div class="inner">
                                <h4>Por favor corrige los siguiente errores:</h4>
                                <ul> </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="control">
                        <input type="submit" value="Enviar" role="form" class="control-submit"/>
                        @if(get_field('autofin_model', $model))
                        <input type="button" value="Evalúame" role="form" class="autofin-evaluate control-submit" style="display: none" />
                        @endif
                </div>

                <input type="hidden" name="url_cotizacion" value="/cotizar/?model={{$model}}">
                <input type="hidden" name="action" value="cotizacion">
                <input type="hidden" name="model" value="{{{get_the_title($model)}}}">
                <input type="hidden" name="autofin_model" value="{{{get_field('autofin_model', $model)}}}">                
                <input type="hidden" name="model_number" value="{{{get_field('numero_de_modelo', $model)}}}">
                <input type="hidden" name="value_vehicle" value="{{{get_field('precio_normal', $model)}}}">
                <input type="hidden" name="brand_vehicle" value="{{SITE_NAME}}">
                <input type="hidden" name="value_bono" value="{{{get_field('precio_normal', $model) - get_field('precio_con_bono', $model)}}}">
                <input type="hidden" name="value_total" value="{{{get_field('precio_con_bono', $model)}}}">

            </form>
        </div>
    </div>
</div>
