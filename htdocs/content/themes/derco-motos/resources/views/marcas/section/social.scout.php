@if(get_field('redes_sociales', 'option'))

	<?php isset($list) ? : $list = false ?>

	{{$list ? '<ul>' : ''}}
		@while(has_sub_field('redes_sociales', 'option'))
			{{$list ? '<li>' : ''}}
				<a href="{{get_sub_field('url', 'option')}}" target="_blank" title="{{{get_sub_field('titulo', 'option')}}}" class="{{$list ? '' : 'social'}}">
					{{get_sub_field('icono', 'option')}}
				</a>
			{{$list ? '</li>' : ''}}
		@endwhile
	{{$list ? '</ul>' : ''}}

@endif