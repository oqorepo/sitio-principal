<section class="newsletter">
	<div class="background get-image">
		<div class="image">
			<img src="{{bfiThumb::always(get_field('imagen_de_fondo_newsletter', 'option'), ['width' => 1420, 'height' => 500, 'quality' => 60])}}" >
		</div>
	</div>
	<div class="content">
		<div class="box">
			<div class="limit">
				<div class="box-content">
					<h2 class="title-line">{{{get_field('titulo_newsletter', 'option')}}}</h2>
					<div class="excerpt">
						{{get_field('bajada_newsletter', 'option')}}
					</div>
					<?php

						if(SITE_NAME == 'suzuki'):
							$id_list_form_action = '43cc8cfae9';

						elseif(SITE_NAME == 'zongshen'):
							$id_list_form_action = '7a96bf1c31';

						elseif(SITE_NAME == 'kymco'):
							$id_list_form_action = '6735b5186c';
							
						elseif(SITE_NAME == 'royal'):
							$id_list_form_action = 'd7c47dbd93';

						endif;

					?>
					<form action="//dercomotos.us1.list-manage.com/subscribe/post?u=330a0a6bbb1cac515b9e07a07&id={{$id_list_form_action}}" method="post" class="noajax" target="_blank">
						<input type="email" value="" name="EMAIL" >
						<input type="submit" value="Enviar">
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

