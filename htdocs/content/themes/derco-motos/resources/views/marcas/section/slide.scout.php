@if(get_field('slide'))
    <section class="home-slider">
        <div class="background"></div>
        <div class="content">

            @if(SITE_NAME == 'suzuki')
                <div class="triangles">
                    <img src="{{themosis_assets()}}/img/triangles.png" alt="">
                </div>
            @endif

            <ul class="slider slick">

                @while(has_sub_field('slide'))

                    <li class="slider-item {{get_sub_field('posicion_del_texto')}}">
                        <div class="slider-background get-image" style="background-color: {{get_sub_field('superposicion_de_color')}};">
                            <div class="image" style="opacity: {{get_sub_field('opacidad_de_color')}};">
                                <img src="{{bfiThumb::always(get_sub_field('imagen')['url'],['width' => 1300, 'height' => 770, 'crop' => true])}}" alt="{{get_sub_field('imagen')['alt']}}">
                            </div>
                        </div>
                        <div class="slider-content">
                            <div class="box">
                                <div class="limit">
                                    <div class="box-content" data-equalize="slider-box-content">
                                        <h2 class="title-line">{{{get_sub_field('titulo')}}}</h2>
                                        <div class="excerpt">{{get_sub_field('descripcion')}}</div>
                                        @if(!empty(get_sub_field('boton')[0]))
                                            <?php $target = get_sub_field('abrir_en_nueva_ventana'); ?>
                                            <div class="buttons" target="{{{isset($target)?'_blank':''}}} ">
                                                <a href="{{get_sub_field('url_boton')}}" class="button">{{{get_sub_field('texto_boton')}}}</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                @endwhile

            </ul>
        </div>
    </section>

@endif