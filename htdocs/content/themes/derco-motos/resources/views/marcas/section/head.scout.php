<head>
	<?php $blog_id = get_current_blog_id(); ?>
	
	<?php if ($blog_id == 2) { ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-56FP69M');</script>
	<!-- End Google Tag Manager -->
	<?php } else if ($blog_id == 3) { ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TSHZ8R5');</script>
	<!-- End Google Tag Manager -->
	<?php } else if ($blog_id == 4) { ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W2DQPK4');</script>
	<!-- End Google Tag Manager -->
	<?php } else if ($blog_id == 5) { ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K3RVS49');</script>
	<!-- End Google Tag Manager -->
	<?php } ?>

	<!-- Default Meta -->
	{{ isset($title) ? '<title >' . $title . '</title>' : ''}}
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">

	<?php wp_head() ?>

	<!-- Favicon-->
	<link rel="apple-touch-icon" sizes="57x57" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/favicon-16x16.png">
	<link rel="manifest" href="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{themosis_assets()}}/favicon-{{{SITE_NAME}}}/ms-icon-144x144.png">
	<meta name="theme-color" content="#1a1a1a">

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->


</head>
<?php 
if (current_user_can('manage_options')){
	$blog_id = get_current_blog_id();

}

?>