<?php
	$query = new WP_Query( ['post_type' => 'post', 'posts_per_page' => 6] );
?>
@if($query->have_posts())
	<section class="home-news">
		<div class="background"></div>
		<div class="content">
			<div class="inner">
				<div class="limit">
					<div class="title">
						<h2 class="title-line">Novedades</h2>
					</div>
					<div class="items">
						<?php $i=0 ?>
							@while($query->have_posts())
							<?php $query->the_post(); ?>

							<div class="col-lg-4 col-sm-6">
		                        <article class="item animate">
		                          <div class="picture get-image">
		                          	<a href="{{get_the_permalink()}}" class="image">
		                          		<img src="{{bfiThumb::always(get_field('imagen_compartir')['url'], ['width' => 360, 'height' => 190])}}" alt="{{get_field('imagen_compartir')['alt']}}">
		                          	</a>
		                          </div>
		                          <div class="info">
		                            <div class="inner">
		                              <div class="date"><i class="fa fa-clock-o"> </i>{{{get_the_date()}}}
		                              </div>
		                              <h2 data-equalize="home-news-item-item">{{get_the_title()}}</h2>
		                              <div class="excerpt">
		                                <p>{{get_field('bajada')}}</p>
		                              </div>
		                              <div class="buttons"><a href="{{get_the_permalink()}}"><i class="fa fa-plus"> </i>Ver más</a></div>
		                            </div>
		                          </div>
		                        </article>
		                      </div>
							<?php $i++ ?>
						@endwhile
					</div>
					<div class="buttons">
						<a href="{{get_the_permalink('13')}}" class="button">Ver todas las noticias</a>
					</div>
				</div>
			</div>
		</div>
	</section>
@endif