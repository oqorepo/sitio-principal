<?php wp_reset_query(); ?>

@if(isset($title) && $title == true)
	@loop
		<div class="title">
			<div class="title-content">
				<h2>{{{Loop::title()}}}</h2>
				<div class="excerpt">
					{{ Loop::content() }}
				</div>
			</div>
		</div>
	@endloop
@else
	<section class="page-title">

		<div class="background get-image" style="background-color: #333333;">
			<div class="image" style="opacity: 1;">
				<?php

					if(get_field('imagen_cabecera')['url']):
						$url_img = get_field('imagen_cabecera')['url'];

					elseif(get_field('imagen_cabecera',get_queried_object()) && is_tax()):
						$url_img = get_field('imagen_cabecera',get_queried_object())['url'];

					elseif(themosis_is_template(['novedades', 'page-template'])):
						$url_img = get_field('imagen_cabecera', get_page_by_path('novedades')->ID)['url'];

					else:
						$url_img =  get_field('imagen_cabecera_titulo', 'option');

					endif;

				?>
				<img src="{{bfiThumb::always($url_img,['width' => 1400, 'height' => 400, 'crop' => true])}}" >
			</div>
		</div>

		<div class="content">
			<div class="inner">
				<div class="limit">
					<div class="breadcrumb">
						<?php
							if(function_exists('yoast_breadcrumb'))
								yoast_breadcrumb('<div class="box">','</div>');
						?>
					</div>
				</div>
			</div>
		</div>

		<?php

			$body_class = get_body_class(true);
			$body_class[2] = empty($body_class[2]) ? false : $body_class[2];

		?>
		@if(SITE_NAME == 'suzuki' && $body_class != 'page-child')
			<div class="triangles">
				<?php

					if(
							themosis_is_template(['novedades', 'page-template']) ||
							themosis_is_template(['concesionarios-marcas', 'page-template'])
					){
						$trinangles_url = themosis_assets() . '/img/triangles-grey.png';
					} else {
						$trinangles_url = themosis_assets() . '/img/triangles.png';
					}

				?>
				<img src="{{$trinangles_url}}">
			</div>
		@endif

		@if(isset($submenu) && $submenu == true)
			<div class="submenu">
				<div class="inner">
					<div class="limit">
						<?php
							wp_nav_menu( [
								'theme_location' => 'concesionarios-zonas'
							]);
						?>
					</div>
				</div>
			</div>
		@endif

	</section>
@endif