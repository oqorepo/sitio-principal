<div class="step step-1 active">
    <div class="inner">
        <div class="limit">
            <div class="model">
                <h2>{{{get_the_title($model)}}} <span>{{{get_field('numero_de_modelo', $model)}}}</span></h2>
            </div>
            <div class="image">
                <img src="{{bfiThumb::always(get_field('imagen_compartir', $model)['url'], ['width' => 700])}}" width="700" alt="{{{get_field('imagen_compartir', $model)['alt']}}}"/>
            </div>
            <div class="boxes">
                <div class="col"> <span>Valor Vehículo</span>
                    <div class="price">{{{format::number(get_field('precio_normal', $model))}}}</div>
                </div>
                @if(get_field('precio_con_bono', $model))
                <div class="col"> <span>Bono</span>
                    <div class="price">{{{format::number(get_field('precio_normal', $model) - get_field('precio_con_bono', $model))}}}</div>
                </div>
                <div class="col"> <span>Valor total</span>
                    <div class="price">{{{format::number(get_field('precio_con_bono', $model))}}}</div>
                </div>
                @endif
                <div class="col autofin-container hidden"> <span> <span class="cae"></span></span>
                    <div class="price"></div>
                    <input type="hidden" name="model" value="{{{get_the_title($model)}}}">
                    <input type="hidden" name="use_autofin" value="{{{get_field('use_autofin', $model)}}}">
                    <input type="hidden" name="autofin_model" value="{{{get_field('autofin_model', $model)}}}">
                    <input type="hidden" name="model_number" value="{{{get_field('numero_de_modelo', $model)}}}">
                    <input type="hidden" name="value_vehicle" value="{{{get_field('precio_normal', $model)}}}">
                    <input type="hidden" name="brand_vehicle" value="{{SITE_NAME}}">
                    <input type="hidden" name="value_bono" value="{{{get_field('precio_normal', $model) - get_field('precio_con_bono', $model)}}}">
                    <input type="hidden" name="value_total" value="{{{get_field('precio_con_bono', $model)}}}">
                </div>
            </div>
            <div class="buttons"> <a href="{{get_site_url()}}/cotizar/?model=0" class="button bluedark">Cotizar otro modelo </a><a href="#" class="button go-formulary">Continuar</a></div>
        </div>
    </div>
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->