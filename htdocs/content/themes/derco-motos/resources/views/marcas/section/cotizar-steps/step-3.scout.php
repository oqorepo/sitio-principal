<div class="step step-3">
    <div class="inner">
        <div class="limit">
            <h2>Estimado <span class="name"></span></h2>
            <div class="excerpt">
                <p>Tu cotización del vehículo modelo: <span class="model"></span> fue enviada exitosamente. Pronto uno de nuestros ejecutivos te contactará.</p>
            </div>
            <div class="message">
                <p>*  Los datos personales recogidos en este formulario, se utilizarán exclusivamente para efectos de cotización.</p>
            </div>
        </div>
        <div class="buttons">
            <a href="{{get_site_url()}}" class="button bluedark">Volver al inicio</a>
            <a href="#" target="_blank" class="button bluedark download-pdf"><i class="fa fa-download"></i>Descargar Cotización</a>
            <a href="{{get_site_url()}}/cotizar/?model=0" class="button go-init">Realizar otra cotización</a>
        </div>
    </div>
</div>