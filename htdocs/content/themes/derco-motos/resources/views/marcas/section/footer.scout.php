@if (get_field('quote_botton', 'option'))
    <div id="quotation"><a href="{{ get_field('quote_url', 'option') }}"><span class="icon"> <i class="fa fa-usd"></i></span><span class="text">Cotiza tu moto</span></a></div>
@endif
<footer>
    <div class="footer">
        <section class="top">
            <div class="inner">
                <div class="limit">
                    <div class="logo">
                        <img src="{{get_field('logo_footer','option')['url']}}" alt="{{get_field('logo_footer','option')['alt']}}">
                    </div>
                </div>
            </div>
        </section>
        <section class="middle">
            <div class="inner">
                <div class="limit">
                    <div class="col phone">
                        <i class="fa fa-phone"></i>
                        <a href="tel:{{{get_field('telefono_1', 'option')}}}">{{{get_field('telefono_1', 'option')}}}</a>
                        <a href="tel:{{{get_field('telefono_2', 'option')}}}">{{{get_field('telefono_2', 'option')}}}</a>
                    </div>
                    <div class="col buttons">
                        <a href="{{ get_permalink(get_field('pagina_de_contacto', 'option')->ID) }}" class="button">
                            {{{get_field('boton_de_contacto_texto', 'option')}}}
                        </a>
                    </div>
                    <div class="col logo">
                        @include('marcas.section.social')
                        <a href="http://www.dercomotos.cl/" target="_blank">
                            <img src="{{get_field('logo_footer_derco', 'option')['url']}}" alt="{{get_field('logo_footer_derco', 'option')['alt']}}">
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="links">
            <div class="inner">
                <div class="limit"> 
                    <ul>
                        @if (get_field('links_footer_items', 'option'))
                            @while (has_sub_field('links_footer_items', 'option'))
                                @if (get_sub_field('icon_type', 'option') == 'font')
                                    <li>
                                        <a href="{{ get_sub_field('link', 'option') }}">
                                            <span class="icon"><i class="fa {{ get_sub_field('font_awesome', 'option') }}"></i></span><span>{{ get_sub_field('text', 'option') }}</span>
                                        </a>
                                    </li>
                                @elseif (get_sub_field('icon_type', 'option') == 'img')
                                    <li>
                                        <a href="{{ get_sub_field('link', 'option') }}">
                                        <span class="icon"><img src="{{ get_sub_field('icon', 'option')['url'] }}" alt="{{ get_sub_field('icon', 'option') ? 'Derco Motos' : '' }}"></span><span>{{ get_sub_field('text', 'option') }}</span>
                                        </a>
                                    </li>
                                @endif
                            @endwhile
                        @endif
                    </ul>
                </div>
            </div>
        </section>
        <section class="bottom">
            <div class="inner">
                <div class="limit">
                    <p>{{{get_field('derechos_reservados', 'option')}}}</p>
                </div>
            </div>
        </section>
    </div>
</footer>

<?php wp_footer() ?>