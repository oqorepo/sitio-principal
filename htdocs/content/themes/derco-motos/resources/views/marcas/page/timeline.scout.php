@extends('marcas.layouts.main')

@section('main')

	<div class="archive-news">

		@include('marcas.section.header-title')

		<section class="timeline">
			<div class="content">
				<div class="inner">
					<div class="limit">

						@include('marcas.section.header-title', ['title' => true])

						<div class="timeline-years">
							<div class="timeline-central"></div>
							<a href="#" data-goto="a.action.down" class="action down"><i class="fa fa-angle-down"></i></a>
							<div class="items">
								@if(get_field('timeline'))
									@while(has_sub_field('timeline'))
										<div class="item">
											<div class="item-wrapper">
												<div class="timeline-line">
													<div class="circle"></div>
												</div>
												<div class="item-content">
													<div class="image">
														<img src="{{bfiThumb::always(get_sub_field('imagen')['url'],['width' => 500, 'height' => 300, 'crop' => true])}}" width="500" />
													</div>
													<div class="info">
														<div class="inner">
															<div class="year">{{{get_sub_field('anio')}}}</div>
															<div class="paragraph">
																{{get_sub_field('descripcion')}}
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									@endwhile
								@endif
							</div>
							<a href="#" data-goto="a.action.down" class="action up">Volver arriba</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

@overwrite