@extends('marcas.layouts.main')

@section('main')

<section class="models">
	<div class="content">
		<div class="inner">
			<div class="limit">
				<div class="items">
					<div class="container">
						@foreach(get_categories(array('taxonomy' => 'tipos_modelos')) as $category)
								<article class="item">
									<div class="article-background get-image">
										<div class="image">
											<img src="{{bfiThumb::always($imagen_destacada = get_field('imagen_compartir', $category)['url'], ['width' => 380, 'height' => 270] )}}" alt="{{get_field('imagen_compartir', $category)['alt']}}">
										</div>
									</div>
									<div class="article-content">
										<h2>
											<a href="{{bloginfo('url') . '/?taxonomy=' . $category->taxonomy . '&term=' . $category->slug}}" title="{{$category->cat_name}}">
												<span class="title-line white">{{$category->cat_name}}</span>
											</a>
										</h2>
									</div>
								</article>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@overwrite