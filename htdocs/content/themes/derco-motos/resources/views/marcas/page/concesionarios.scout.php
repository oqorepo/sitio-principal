@extends('marcas.layouts.main')

@section('main')

    @include('marcas.section.header-title')

    <?php
        $query = new WP_Query_Multisite( ['post_type' => 'page', 'sites' => [ 'sites__in' => [1] ], 'p' => 262  ] );
            while( $query->have_posts() ) : $query->the_post();
    ?>
    
    <section class="dealers">
        <div class="content">
            <div class="inner">
                <div class="limit">
                    <div class="content-sidebar">
                        <div id="accordion">
                            @while(have_rows('region_zn')) <?php the_row(); ?>
                                <h3 class="accordion-title">{{{get_sub_field('nombre_region_zn')}}}</h3>
                                <div class="accordion-content">
                                    <ul>
                                    <?php
                                        $nombre_comuna_ant[] =  '';

                                        while (have_rows('comunas_zn')) : the_row();
                                            $nombre_comuna = get_sub_field('nombre_comuna_zn');

                                            while (have_rows('concesionarios_zn')) : the_row();
                                                $marca_actual = false;
                                                foreach (get_sub_field('marcas') as $marca){
                                                    if($marca == SITE_NAME)
                                                        $marca_actual = true;
                                                }

                                                if($marca_actual){
                                                    $nombre_comuna_ant[] =  $nombre_comuna;
                                                    if($nombre_comuna_ant[count($nombre_comuna_ant) -2] != $nombre_comuna){
                                                    ?>
                                                        <li data-box="{{helpers::slugify($nombre_comuna, '-')}}"><a href="#">{{{$nombre_comuna}}}</a></li>
                                                    <?php
                                                    }
                                                }
                                            endwhile;
                                        endwhile;
                                    ?>
                                    </ul>
                                </div>
                            @endwhile
                        </div>
                    </div>

                    <div class="content-page">
                        <div class="info" id="info-main">
                            <div class="inner">
                                <h2>Concesionarios</h2>
                                <div class="paragraph">
                                    <p>Selecciona una zona en la barra izquierda y luego tu concesionario más cercano.</p>
                                </div>
                            </div>
                        </div>
                        @while(have_rows('region_zn')) <?php the_row(); ?>
                            @while(have_rows('comunas_zn')) <?php the_row(); ?>

                                <?php $nombre_comuna = get_sub_field('nombre_comuna_zn'); ?>

                                @while(have_rows('concesionarios_zn')) <?php the_row(); ?>

                                    <?php $marca_actual = false; ?>


                                    @if(in_array(SITE_NAME, get_sub_field('marcas')))
                                        <div class="box" data-show="{{helpers::slugify($nombre_comuna, '-')}}">
                                            <div class="inner">
                                            <div class="title">
                                                <h2>{{{get_sub_field('nombre_concesinario')}}}</h2>
                                            </div>
                                            <div class="cols">
                                                <div class="col">
                                                    <div class="paragraph">
                                                        {{get_sub_field('descripcion')}}
                                                    </div>
                                                    <div class="info">
                                                        <div class="phone">
                                                            <h3>Dirección</h3>
                                                                <a href="#">{{{ get_sub_field('direccion')['address'] }}}</a>
                                                            </a>
                                                        </div>
                                                        <div class="phone">
                                                            <h3>Teléfonos</h3>
                                                                <a href="tel:{{get_sub_field('telefono')}}">{{{get_sub_field('telefono')}}}</a>
                                                                @if(get_sub_field('telefono_celular'))
                                                                    <a href="tel:+56{{get_sub_field('telefono_celular')}}">{{{get_sub_field('telefono_celular')}}}
                                                                @endif
                                                            </a>
                                                        </div>
                                                        <div class="contact">
                                                            <h3>Contacto</h3><a href="mailto:{{get_sub_field('email')}}">{{{get_sub_field('email')}}}</a>
                                                        </div>
                                                        <div class="services">
                                                            <h3>Servicios</h3>
                                                            <div class="items">

                                                                @foreach( get_sub_field('servicios') as $servicio )

                                                                    @if($servicio == 'ventas')
                                                                        <div class="item">
                                                                            <img src="{{themosis_assets()}}/img/service-icons/{{SITE_NAME}}/ventas.svg" data-equalize="icon-service">
                                                                            <span>Ventas</span>
                                                                        </div>
                                                                    @elseif($servicio == 'servicios')
                                                                        <div class="item">
                                                                            <img src="{{themosis_assets()}}/img/service-icons/{{SITE_NAME}}/servicios.svg" data-equalize="icon-service" >
                                                                            <span>Servicios</span>
                                                                        </div>
                                                                    @elseif($servicio == 'repuestos')
                                                                        <div class="item">
                                                                            <img src="{{themosis_assets()}}/img/service-icons/{{SITE_NAME}}/repuestos.svg" data-equalize="icon-service">
                                                                            <span>Repuestos</span>
                                                                        </div>
                                                                    @endif

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="map">
                                                        <div class="googlemap">
                                                            <div data-lat="{{get_sub_field('direccion')['lat']}}" data-lng="{{get_sub_field('direccion')['lng']}}" data-pin="{{themosis_assets()}}/img/pin.png" data-pin-width='38' data-pin-height='48' class="marker">
                                                                <h4>{{{get_sub_field('nombre_concesinario')}}}</h4>
                                                                <p class="address">{{{get_sub_field('direccion')['address']}}}</p>
                                                                <p>
	                                                                <i class="fa fa-phone" aria-hidden="true"></i> {{{get_sub_field('telefono')}}}
	                                                                @if(get_sub_field('telefono_celular'))
	                                                                     - <i class="fa fa-mobile" aria-hidden="true"></i> {{{get_sub_field('telefono_celular')}}}
                                                                    @endif
                                                                </p>
                                                                <p> <a href="http://maps.google.com?q={{ get_sub_field('direccion')['lat'] }},{{ get_sub_field('direccion')['lng'] }}" target="_blank"> <i class="fa fa-map-marker"> </i>Ver en Google Maps</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endwhile
                            @endwhile
                        @endwhile
                    </div>

                </div>
            </div>
        </div>
    </section>

    <?php
        endwhile;
            wp_reset_postdata();

    ?>

@overwrite