@extends('marcas.layouts.main')

@section('main')

<div class="archive-news">
	@include('marcas.section.header-title')
	<section class="home-news">
		<div class="background"></div>
		<div class="content">
			<div class="title">
                  <div class="container">
                    <div class="row"> 
                      <div class="col-lg-12">
                        <h1 class="title-line">Novedades</h1>
                      </div>
                    </div>
                  </div>
                </div>
					<div class="items ">
					  <div class="container">
                    	<div class="row">
						@loop
						<div class="col-lg-4">
							<article class="item animate">
	                          <div class="picture get-image"><a href="{{get_permalink()}}" class="image">
	                          	<img src="{{bfiThumb::always(get_field('imagen_compartir')['url'], ['width' => 360, 'height' => 190])}}" alt="{{get_field('imagen_compartir')['alt']}}">
	                          </a></div>
	                          <div class="info">
	                            <div class="inner">
	                              <div class="date"><i class="fa fa-clock-o"> </i>{{get_the_date()}}
	                              </div>
	                              <h2 data-equalize="home-news-item-item">{{{Loop::title()}}}</h2>
	                              <div class="excerpt">
	                                {{get_field('bajada')}}
	                              </div>
	                              <div class="buttons"><a href="{{get_permalink()}}"><i class="fa fa-plus"> </i>Ver más</a></div>
	                            </div>
	                          </div>
	                        </article>
						</div>
						@endloop
						</div>
						</div>
					</div>
		</div>
	</section>
</div>

@overwrite