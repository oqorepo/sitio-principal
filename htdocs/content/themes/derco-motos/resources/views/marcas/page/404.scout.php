@extends('marcas.layouts.main')

@section('main')

    <div class="archive-news">
        <section class="page-title">

            <div class="background get-image" style="background-color: #333333;">
                <div class="image" style="opacity: 1;">
                    <?php

                        $url_img =  get_field('imagen_cabecera_titulo', 'option');

                    ?>
                    <img src="{{bfiThumb::always($url_img,['width' => 1400, 'height' => 400, 'crop' => true])}}" >
                </div>
            </div>

            <div class="content">
                <div class="inner">
                    <div class="limit">
                        <div class="breadcrumb">
                            <?php
                            if(function_exists('yoast_breadcrumb'))
                                yoast_breadcrumb('<div class="box">','</div>');
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php

            $body_class = get_body_class(true);
            $body_class[2] = empty($body_class[2]) ? false : $body_class[2];

            ?>
            @if(SITE_NAME == 'suzuki' && $body_class != 'page-child')
                <div class="triangles">
                    <?php

                    if(
                            themosis_is_template(['novedades', 'page-template']) ||
                            themosis_is_template(['concesionarios-marcas', 'page-template'])
                    ){
                        $trinangles_url = themosis_assets() . '/img/triangles-grey.png';
                    } else {
                        $trinangles_url = themosis_assets() . '/img/triangles.png';
                    }

                    ?>
                    <img src="{{$trinangles_url}}">
                </div>
            @endif


        </section>
        <section class="history">
            <div class="content">
                <div class="inner">
                    <div class="limit">
                        <div class="title">
                            <h2 class="title-line">404</h2>
                        </div>
                        <div class="paragraph">
                            <p style="text-align: center;">Página no encontrada.
                            <p style="text-align: center;"><a href="{{get_site_url()}}" title="Volver al sitio">Volver al sitio</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@overwrite