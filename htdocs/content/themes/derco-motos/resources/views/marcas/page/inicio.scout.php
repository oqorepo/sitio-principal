@extends('marcas.layouts.main')

@section('main')

<div class="page-home">

	@include('marcas.section.slide')

	@if(have_rows('modelos_destacados'))
	<section class="home-hightlights">
		<div class="background"> </div>
		<div class="content">
			<div class="inner">
				<div class="limit">
					<div class="title">
						<h2 class="title-line">Destacados</h2>
					</div>
					<div class="items">

						@foreach(get_field('modelos_destacados') as $page)

						<article class="item animate">
							<div class="name" data-equalize="item-name">
								<h2>{{{$page->post_title}}}</h2>
								@if(get_field('numero_de_modelo', $page->ID))
								<h3>{{'('.get_field('numero_de_modelo', $page->ID).')'}}</h3>
								@endif
							</div>
							<div class="image" data-equalize="img-hightlights">
								<?php
								if (!empty(get_field('imagen_destacada_header', $post->ID)['url']))
									$field = 'imagen_destacada_header';
								else
									$field = 'imagen_compartir';
								?>
								<img src="{{bfiThumb::always(get_field($field, $page->ID)['url'],['width' => 270, 'crop' => true])}}" alt="{{{get_field($field, $page->ID)['alt']}}}" width="270" height="150"/>
							</div>
							{{--
								<div data-equalize="home-hightlights-excerpt" class="excerpt">
									<p>{{get_field('descripcion_breve', $page->ID)}}</p>
								</div>
								--}}
								@if(get_field('precio_con_bono', $page->ID))
								<div class="bono">
									<h3>Precio con bono</h3><span>{{{format::number(get_field('precio_con_bono', $page->ID))}}}</span>
								</div>
								@else
								<div class="price">
									<h3>Precio normal</h3><span>{{{format::number(get_field('precio_normal', $page->ID))}}}</span>
								</div>
								@endif
								<div class="autofin-container price hidden">
									<h3 class="cae"></h3>
									<span class="price"></span>
									<input type="hidden" name="use_autofin" value="{{get_field('use_autofin', $page->ID)}}">
									<input type="hidden" name="autofin_model" value="{{get_field('autofin_model', $page->ID)}}">
									<input type="hidden" name="model_number" value="{{get_field('numero_de_modelo', $page->ID)}}">
									<input type="hidden" name="value_vehicle" value="{{get_field('precio_normal', $page->ID)}}">
									<input type="hidden" name="brand_vehicle" value="{{SITE_NAME}}">
								</div>
								<div class="buttons"> <a href="{{get_the_permalink($page->ID)}}" class="button">Ver más</a></div>
							</article>

							@endforeach

						</div>
					</div>
				</div>
			</div>
		</section>
		@endif

		@include('marcas.section.novedades')

		@include('marcas.section.newsletter')

	</div>

	@overwrite