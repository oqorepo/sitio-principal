@extends('marcas.layouts.main')

@section('main')

<div class="archive-news">
@loop
	<section class="models-introduce">
		<div class="background get-image">
			<div class="image">
				<img src="{{bfiThumb::always(get_field('imagen_destacada_header')['url'], ['width' => 1450, 'height' => 650] )}}" alt="{{get_field('imagen_destacada_header')['alt']}}">
			</div>
		</div>
		<div class="content">
			<div class="inner">
				<div class="limit">
					<div class="name">
						<h2 class="title-line">{{{get_the_title()}}}</h2>
						<h3>
							@if(get_field('numero_de_modelo'))
							{{'('.get_field('numero_de_modelo').')'}}<br><br>
							@endif
						</h3>
					</div>
					<div class="cols">
						<div class="col">
							<div class="title black">Precio Normal </div>
							<div class="price">{{{format::number(get_field('precio_normal'))}}}</div>
						</div>
						@if(get_field('precio_con_bono'))
						<div class="col">
							<div class="title blue">Bono</div>
							<div class="price">{{{format::number(get_field('precio_normal') - get_field('precio_con_bono'))}}}</div>
						</div>
						<div class="col">
							<div class="title red">Precio con bono</div>
							<div class="price">{{{format::number(get_field('precio_con_bono'))}}}</div>
						</div>
						@endif

						<div class="col autofin-container hidden"> 
							<div class="title black"><span class="cae"></span></div>
							<div class="price"></div>
							<input type="hidden" name="model" value="{{{get_the_title(Loop::ID())}}}">
							<input type="hidden" name="use_autofin" value="{{{get_field('use_autofin', Loop::ID())}}}">
							<input type="hidden" name="autofin_model" value="{{{get_field('autofin_model', Loop::ID())}}}">
							<input type="hidden" name="model_number" value="{{{get_field('numero_de_modelo', Loop::ID())}}}">
							<input type="hidden" name="value_vehicle" value="{{{get_field('precio_normal', Loop::ID())}}}">
							<input type="hidden" name="brand_vehicle" value="{{SITE_NAME}}">
							<input type="hidden" name="value_bono" value="{{{get_field('precio_normal', Loop::ID()) - get_field('precio_con_bono', Loop::ID())}}}">
							<input type="hidden" name="value_total" value="{{{get_field('precio_con_bono', Loop::ID())}}}">
						</div>

						<div class="col"><a href="{{get_site_url()}}/cotizar/?model={{get_the_ID()}}" class="button red">Cotizar</a></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@if (get_field('caracteristicas_tecnicas_repeater') || get_field('model_colors')) 
	<section class="models-single">
		<div class="background"></div>
		<div class="content">
			<div class="inner">
				<div class="limit">
					<div class="title">
						<div class="title-content">
							<h2 class="title-line">Características</h2>
							<h3>{{{get_field('titulo')}}}</h3>
							<div class="paragraph">
								{{get_field('descripcion')}}
							</div>
							@if(have_rows('caracteristicas_tecnicas_repeater'))
							<div class="icons">
								@while(have_rows('caracteristicas_tecnicas_repeater')) <?php the_row(); ?>
								<div class="icon">
									<div class="image">
										<img src="{{themosis_assets()}}/img/characteristics-icons/{{SITE_NAME}}/{{get_sub_field('mostrar_icono_de')}}.svg" width="70" height="65">
										{{-- @if (SITE_NAME == 'suzuki')
										@else
										<img src="{{ get_sub_field('mostrar_icono_de')['url'] }}" width="70" height="65">
										@endif --}}
									</div>
									<p>{{get_sub_field('texto')}}</p>
								</div>
								@endwhile
							</div>
							@else
							<div class="icons">
								@foreach( get_field('caracteristicas_tecnicas') as $field)
								@if($field == 'origen-japon')
								<div class="icon">
									<div class="image">
										<img src="{{themosis_assets()}}/img/characteristics-icons/origen.png"/>
									</div>
									<p>origen japón</p>
								</div>
								@elseif($field == '6-cambios')
								<div class="icon">
									<div class="image">
										<img src="{{themosis_assets()}}/img/characteristics-icons/6-cambios.png"/>
									</div>
									<p>6 cambios</p>
								</div>
								@elseif($field == 'refrigerante-liquido')
								<div class="icon">
									<div class="image">
										<img src="{{themosis_assets()}}/img/characteristics-icons/refrigerante.png"/>
									</div>
									<p>refrigerante líquido</p>
								</div>
								@elseif($field == 'economia')
								<div class="icon">
									<div class="image">
										<img src="{{themosis_assets()}}/img/characteristics-icons/economia.png"/>
									</div>
									<p>económia</p>
								</div>
								@endif
								@endforeach
							</div>
							@endif
							<?php $colors = get_field('model_colors'); ?>
							@if ($colors)
							<div class="colors">
								<h3>colores disponibles</h3>
								<div class="items">
									@foreach ($colors as $color)
									<div style="background-color:{{ $color['color'] }}" class="item"></div>
									@endforeach
								</div>
							</div>
							@endif

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif

	@if(have_rows('tabla'))
	<section class="models-tables">
		<div class="background get-image">
			<div class="image">
				@if(get_field('imagen_de_fondo')['url'])
				<img src="{{bfiThumb::always(get_field('imagen_de_fondo')['url'], ['width' => 1450, 'height' => 870, 'quality' => 60] )}}" alt="{{get_field('imagen_de_fondo')['alt']}}">
				@else
				<img src="{{bfiThumb::always(get_field('imagen_de_fondo_tabla_de_datos', 'option'), ['width' => 1450, 'height' => 870, 'quality' => 60] )}}" >
				@endif
			</div>
		</div>
		<div class="content">
			<div class="inner">
				<div class="limit">
					<div class="title">
						<div class="title-content">
							<h2 class="title-line white">Ficha técnica</h2>
						</div>
					</div>
					<div class="items">
						<div class="slider">
							<ul class="slick" data-cols="2">
								@foreach(get_field('tabla') as $tabla)
								<li>
									<article class="item element">
										<div class="box-content">
											<div class="box">
												<h2><span>{{{$tabla['titulo']}}}</span></h2>
												<table>
													<tbody>
														@foreach($tabla['item'] as $item)
														<tr>
															<td class="name"><span>{{{$item['item_nombre']}}}</span></td>
															<td class="value"><span>{{{$item['detalle']}}}</span></td>
														</tr>
														@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</article>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="buttons">
						<a href="{{get_site_url()}}/cotizar/?model={{get_the_ID()}}" class="button red">Cotizar</a>
						<a href="{{get_field('ficha_en_pdf')['url']}}" title="{{{get_field('ficha_en_pdf')['title']}}}" class="button">Descarga Ficha</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif

	@if(have_rows('galeria'))
	<section class="models-gallery">
		<div class="content">
			<div class="inner">
				<div class="limit">
					<div class="title">
						<div class="title-content">
							<h2 class="title-line">Galería</h2>
						</div>
					</div>
					<div class="slider">
						<ul class="slider slider-gallery">
							@foreach(get_field('galeria') as $image)
							<li>
								<img src="{{bfiThumb::always($image['url'], ['width' => 1200, 'height' => 600])}}" alt="{{{$image['alt']}}}">
								@if($image['title'])
								<!-- <h2>{{{$image['title']}}}</h2>  -->
								@endif
							</li>
							@endforeach
						</ul>
					</div>
					<div class="thumbnails">
						<ul class="slider-thumbnails">
							@foreach(get_field('galeria') as $image)
							<li>
								<img src="{{bfiThumb::always($image['url'], ['width' => 180, 'height' => 90])}}" alt="{{{$image['alt']}}}" width="180" height="90">
							</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif

	<section class="models-details">
		<div class="background get-image">
			<div class="image">
				@if(get_field('imagen_de_fondo_prefooter')['url'])
				<img src="{{bfiThumb::always(get_field('imagen_de_fondo_prefooter')['url'], ['width' => 1400, 'height' => 600, 'quality' => 60])}}" alt="{{{get_field('imagen_de_fondo_prefooter')['alt']}}}">
				@else
				<img src="{{bfiThumb::always(get_field('imagen_por_defecto_prefooter', 'option'), ['width' => 1400, 'height' => 600, 'quality' => 60])}}">
				@endif
			</div>
		</div>
		<div class="content">
			<div class="inner">
				<div class="limit">
					<div class="title">
						<div class="title-content">
							<h2 class="title-line white">{{{get_the_title()}}}</h2>
							<!-- <div class="price">{{{format::number(get_field('precio_normal'))}}}</div> -->
						</div>
					</div>
					<div class="cols">
						<div class="col" style="float: none!important; width: inherit; margin-right: 0px!important;">
							<h3>{{{get_field('columna_titulo_izq') ? get_field('columna_titulo_izq') : 'Cotizar'}}}</h3>
							<div class="excerpt">
								{{get_field('columna_descripcion_izq')}}
							</div>
							<div class="buttons">
								<?php $url = get_field('url_tipo_izq') == 'url' ? get_field('url_izq') : get_field('pagina_izq')->guid;  ?>
								<a href="{{ empty($url) ? get_site_url() . '/cotizar/?model=' . get_the_ID()  : $url }}"
								target="{{ get_field('abrir_en_izq') == 'nueva_ventana' ? '_blank' : '' }}" class="button red"
								>{{get_field('boton_izq') ? get_field('boton_izq') : 'Cotizar'}}</a>
							</div>
						</div>
						<div class="col" style="display: none!important">
							<h3>{{{get_field('columna_titulo_der') ? get_field('columna_titulo_der') : 'Más Información'}}}</h3>
							<div class="excerpt">
								{{get_field('columna_descripcion_der')}}
							</div>
							<div class="buttons">
								<?php $url = get_field('url_tipo_der') == 'url' ? get_field('url_der') : get_field('pagina_der')->guid;  ?>
								<a href="{{ empty($url) ? 'http://clicktocall.in-touch.cl/index.aspx?nEmpresa=20' : $url }}"
								target="{{ get_field('abrir_en_der') == 'nueva_ventana' || empty(get_field('abrir_en_der')) ? '_blank' : '' }}" class="button red"
								>{{get_field('boton_der') ? get_field('boton_der') : 'Te llamamos gratis'}}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
@endloop
@overwrite