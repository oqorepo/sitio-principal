@extends('marcas.layouts.main')

@section('main')


<div class="archive-news">
	@include('marcas.section.header-title')
	<section class="contact">
		<div class="content">
			<div class="inner">
				<div class="limit">
					<form action="{{helpers::ajax_url()}}" method="post">
						@include('marcas.section.header-title', ['title' => true])
						<div class="fields">
							<div class="field">
								<div class="field-2">
									<div class="control fullname text">
										<label for="fullname" class="control-label">Nombre</label>
										<input class="control-input" type="text" maxlength="200" id="fullname_28521" name="fullname" required="required" data-msg="Ingresa tu nombre"/>
									</div>
								</div>
								<div class="field-2">
									<div class="control rut rut">
										<label for="rut" class="control-label">Rut</label>
										<input class="control-input rut" type="text" id="rut_41812" name="rut" minlength="7" required="required" data-msg="Ingresa un rut válido"/>
									</div>
								</div>
							</div>
							<div class="field">
								<div class="field-2">
									<div class="control email email">
										<label for="email" class="control-label">E-mail</label>
										<input class="control-input" type="email" maxlength="160" id="email_34912" name="email" required="required" data-msg="Ingresa un correo electrónico válido"/>
									</div>
								</div>
								<div class="field-2">
									<div class="control phone phone">
											<label for="phone" class="control-label">Teléfono</label>
										<input class="control-input" type="number" maxlength="9" id="phone_25571" name="phone" required="required" data-msg="Ingresa un teléfono de contacto"/>
									</div>
								</div>
							</div>
							<div class="field">
								<div class="control message textarea">
									<label for="message" class="control-label">Mensaje</label>
									<textarea class="control-textarea" maxlength="1500" type="text" id="message_53900" name="message" placeholder="" required="required" data-msg="Escríbanos un mensaje"></textarea>
								</div>
							</div>
						</div>
						<div class="fields errors">
							<div class="control-error">
								<div class="errors">
									<div class="inner">
										<h4>Por favor corrige los siguiente errores:</h4>
										<ul> </ul>
									</div>
								</div>
							</div>
						</div>
						<div class="fields">
							<div class="control">
								<input type="submit" value="Enviar" role="form" class="control-submit"/>
							</div>
						</div>
						<input type="hidden" name="action" value="contacto">
						<input type="hidden" name="send_text" value="{{{get_field('texto_para_mensaje_enviado', 'option')}}}">
					</form>
				</div>
			</div>
		</div>
	</section>
	<section class="banner">
		<div class="background get-image">
			<div class="image">
				<img src="{{bfiThumb::always(get_field('imagen_de_fondo_prefooter'), ['width' => 1400, 'height' => 500] )}}" >
			</div>
		</div>
		<div class="content">
			<div class="box">
				<div class="limit">
					<div class="box-content">
					@if(get_field('logo_top')['url'])
						<img src="{{bfiThumb::always(get_field('logo_top')['url'], ['height' => 60])}}" alt="{{get_field('logo_top')['alt']}}" style="margin: 20px 0 30px" class="logo">
					@endif
						<h2 class="title-line white">{{get_field('titulo_prefooter')}}</h2>
						<div class="excerpt">
							{{get_field('descripcion_prefooter')}}
						</div>
						@if(get_field('mostrar_boton_prefooter') == 'SI')
							<div class="buttons">
								<a href="{{get_field('pagina_prefooter')}}" class="button">{{get_field('boton_titulo_prefooter')}}</a>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

@overwrite