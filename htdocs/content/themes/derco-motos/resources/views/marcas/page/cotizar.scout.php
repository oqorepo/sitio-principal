@extends('marcas.layouts.main')

@section('main')

<?php if( !empty($model)): ?>
    <div class="archive-news">
        <section class="quote-formulary">
            <div class="content">
                <div class="inner">
                    <div class="limit">
                        @include('marcas.section.header-title', ['title' => true])
                        <div class="steps-views">
                            <div class="box">
                                <ul>
                                    <li class="active">1</li>
                                    <li>2</li>
                                    <li>3</li>
                                </ul>
                            </div>
                        </div>
                        <div class="steps-fields">
                            @include('marcas.section.cotizar-steps.step-1')
                            @include('marcas.section.cotizar-steps.step-2')
                            @include('marcas.section.cotizar-steps.step-3')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php else: ?>
    @foreach(get_categories(array('taxonomy' => 'tipos_modelos')) as $category)
        <section class="models-archive quote">
            <div class="background"></div>
            <div class="content">
                <div class="inner">
                    <div class="limit">
                        <div class="title">
                            <div class="title-content">
                                <h2>{{{$category->name}}}</h2>
                                <div class="excerpt">
                                    <p>{{{$category->category_description}}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="items">
                            @query(
                                [
                                    'post_type' => 'modelo',
                                    'orderby' => 'name',
                                    'tax_query' => [
                                        'relation' => 'OR',
                                        [
                                            'taxonomy' => 'tipos_modelos',
                                            'field'    => 'slug',
                                            'terms'    => [$category->slug],
                                        ]
                                    ]
                                ]
                            )
                                <article class="item">
                                    <h2 data-equalize="item-title-{{$category->slug}}">{{{Loop::title()}}}</h2>
                                    <div class="model">{{{get_field('numero_de_modelo')}}}</div>
                                    <div class="image">
                                        <?php
                                        if(get_field('imagen_compartir')['url'])
                                            $url_thumb = get_field('imagen_compartir')['url'];

                                        else
                                            $url_thumb = themosis_assets() . '/img/imagen-no-disponible.jpg';
                                        ?>
                                        <a href="{{get_the_permalink()}}">
                                            <img src="{{bfiThumb::always($url_thumb, ['width' => 250, 'height' => 180] )}}" width="250" height="180"/>
                                        </a>
                                    </div>
                                    <div class="buttons">
                                        <a href="{{get_the_permalink()}}" class="button">Ver más</a>
                                        <a href="{{get_site_url()}}/cotizar/?model={{get_the_ID()}}" class="button bluedark">Cotizar</a>
                                    </div>
                                </article>
                            @endquery
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endforeach
<?php endif; ?>

@overwrite