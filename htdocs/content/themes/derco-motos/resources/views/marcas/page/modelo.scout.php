@extends('marcas.layouts.main')

@section('main')

	<div class="archive-news">

		@include('marcas.section.header-title', ['title' => false])

		<section class="models-archive">
			<div class="background"></div>
			<div class="content">
				<div class="inner">
					<div class="limit">
						<div class="title">
							<div class="title-content">
								<h2 class="title-line">{{{$taxonomy_title}}}</h2>
								<div class="excerpt">
									{{ term_description('', get_query_var('taxonomy')) }}
								</div>
							</div>
						</div>
						<div class="items">
							@query(
								[
									'post_type' => 'modelo',
									'orderby' => 'menu_order',
									'tax_query' => [
										'relation' => 'OR',
										[
											'taxonomy' => 'tipos_modelos',
											'field'    => 'slug',
											'terms'    => [$taxonomy_term],
										]
									]
								]
							)
								<article class="item">
									<h2 data-equalize="item-title">{{{Loop::title()}}}</h2>
									<div class="model">{{{get_field('numero_de_modelo')}}}</div>
									<div class="image">
										@if (get_field('models_new'))
										 	<span>NUEVA</span>
										@endif
										<?php
											if(get_field('imagen_compartir')['url'])
												$url_thumb = get_field('imagen_compartir')['url'];

											else
												$url_thumb = themosis_assets() . '/img/imagen-no-disponible.jpg';
										?>
										<a href="{{get_the_permalink()}}">
											<img src="{{bfiThumb::always($url_thumb, ['width' => 250, 'height' => 180] )}}" width="250" height="180"/>
										</a>
									</div>
									<div class="buttons">
										<a href="{{get_the_permalink()}}" class="button">Ver más</a>
										<a href="{{get_site_url()}}/cotizar/?model={{get_the_ID()}}" class="button bluedark">Cotizar</a>
									</div>
								</article>
							@endquery
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<?php global $post ?>

@overwrite