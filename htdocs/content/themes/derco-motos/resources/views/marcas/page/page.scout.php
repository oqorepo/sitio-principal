@extends('marcas.layouts.main')

@section('main')

    <?php
        if(get_field('desea_redireccionar')):
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . get_field('pagina_para_redireccion')->guid);
        endif;
    ?>

    <div class="archive-news">
        @include('marcas.section.header-title')
        @loop
            <section class="history">
                <div class="content">
                    <div class="inner">
                        <div class="limit">
                            <div class="title">
                                <h2 class="title-line">{{Loop::title()}}</h2>
                            </div>
                            <div class="paragraph">{{Loop::content()}}</div>
                        </div>
                    </div>
                </div>
            </section>
        @endloop
    </div>

@overwrite