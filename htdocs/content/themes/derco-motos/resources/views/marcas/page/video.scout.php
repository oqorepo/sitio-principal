@extends('marcas.layouts.main') @section('main')
<div class="archive-news">
    @include('marcas.section.header-title')
    <div class="page-content">
        <div class="archive-news">
            <section class="archive-videos">
                <div class="background"></div>
                <div class="content">
                    <div class="items">
                        <div class="container">
                            @if (get_field('videos')) 
                                @while (has_sub_field('videos'))
                                <article class="article-video">
                                    <div class="video-image">
                                        <button data-youtube="{{ get_sub_field('video_code') }}" class="video"><img src="{{bfiThumb::always(get_sub_field('video_img')['url'], ['width' => 560, 'height' => 315])}}" alt=""></button>
                                        <div class="video-iframe">
                                            <button class="close"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="video-info">
                                        <div class="info-box">
                                            <h2>{{ get_sub_field('video_title') }}</h2>
                                            <div class="excerpt">
                                                {{ get_sub_field('video_desc') }}
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                @endwhile 
                            @endif
                        </div>
                    </div>
                </div>
            </section>
            <section class="pagination">
                <div class="content">
                    <div class="inner">
                        <div class="limit">
                            <!-- <nav class="pagination"><a href="noticias.html" class="prev page-numbers">« Anterior</a><span class="page-numbers current">1</span><a href="noticias.html" class="page-numbers">2</a><a href="noticias.html" class="page-numbers">3</a><a href="noticias.html" class="page-numbers">4</a><a href="noticias.html" class="page-numbers">5</a><a href="noticias.html" class="page-numbers">6</a><a href="noticias.html" class="page-numbers">7</a><a href="noticias.html" class="page-numbers">8</a><a href="noticias.html" class="page-numbers">9</a><a href="noticias.html" class="page-numbers">10</a><a href="noticias.html" class="page-numbers">11</a><a href="noticias.html" class="page-numbers">12</a><a href="noticias.html" class="page-numbers">13</a><a href="noticias.html" class="page-numbers">14</a><a href="noticias.html" class="page-numbers">15</a><a href="noticias.html" class="next page-numbers">Siguiente »</a></nav> -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@overwrite