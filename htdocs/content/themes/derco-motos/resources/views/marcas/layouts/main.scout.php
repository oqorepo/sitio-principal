<!doctype html>
<html lang="es">

@include('marcas.section.head')

<body {{body_class()}} style="display:none;">
<?php $blog_id = get_current_blog_id(); ?>
    
<?php if ($blog_id == 2) { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-56FP69M"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php } else if ($blog_id == 3) { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSHZ8R5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php } else if ($blog_id == 4) { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W2DQPK4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php } else if ($blog_id == 5) { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K3RVS49"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php } ?>

<div id="{{{SITE_NAME}}}">
    
    <div id="barmenu"><a class="trigger">Trigger<span class="box"> <span class="bar top"></span><span class="bar middle"></span><span class="bar bottom"></span></span></a>
        <nav role="navigation">
            <div class="logo"> 
                <a href="index.html">
                    <img src="{{bfiThumb::always(get_field('logo_header','option')['url'],['width' => 200])}}" alt="{{get_field('logo_footer','option')['alt']}}">
                </a>
            </div>
            <div class="scrolling">
                <div class="menu">
                    <nav>
                        <?php
                            wp_nav_menu( [
                                    'theme_location' => 'header-nav-top'
                            ]);
                        ?>
                    </nav>
                </div>
                <div class="buttons">
                    @if(get_field('menu_links', 'option'))
                        @while(has_sub_field('menu_links', 'option'))
                            <li><a href="{{get_sub_field('url', 'option')}}" target="_blank" title="{{get_sub_field('titulo', 'option')}}">{{get_sub_field('icono', 'option')}} </a></li>
                        @endwhile
                    @endif
                </div>
                <div class="social">
                    <nav>
                        @include('marcas.section.social',  ['list' => true])
                    </nav>
                </div>
            </div>
        </nav>
    </div>
    <header>
        <div class="header">
            @if (wp_get_nav_menu_object('menu-superior')->count > 0)
                <div class="header-top">
                    <div class="inner"> 
                        <div class="limit">
                           <?php
                                wp_nav_menu([
                                        'theme_location' => 'superior-nav'
                                ]);
                            ?>
                        </div>
                     </div>
                </div>
            @endif
            <div class="header-bottom">
                <div class="inner">
                    <div class="limit">
                        <div class="logo">
                            <a href="{{get_site_url()}}">
                                <h1>{{{bloginfo('name')}}}</h1>
                                <img src="{{bfiThumb::always(get_field('logo_header','option')['url'],['width' => 200])}}" alt="{{get_field('logo_footer','option')['alt']}}">
                            </a>
                        </div>
                        <div class="menu">
                            <?php
                                wp_nav_menu( [
                                        'theme_location' => 'header-nav-top'
                                ]);
                            ?>
                        </div>
                        <div class="social">
                            @include('marcas.section.social',  ['list' => true])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="page">
        <div class="page-content">
            @yield('main')
        </div>
    </div>
    @include('marcas.section.footer')
</div>

<div id="loading">
    <i class="fa fa-spinner fa-spin"></i>
</div>

</body>
</html>