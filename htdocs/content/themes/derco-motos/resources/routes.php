<?php

/**
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */


Route::get('single', function()
{
	return View::make('derco.page.single');
});

Route::get('page', function()
{
	return View::make('derco.mixing.template');
});

Route::get('template', ['inicio', function()
{
	return View::make('derco.page.inicio');
}]);

Route::get('template', ['todas-las-noticias', function()
{
	return View::make('derco.page.noticias');
}]);

Route::get('template', ['contacto', function()
{
	return View::make('derco.page.contacto');
}]);

Route::get('template', ['clinica', function()
{
	return View::make('derco.page.clinica');
}]);

Route::get('template', ['escuela', function()
{
	return View::make('derco.page.escuela');
}]);

Route::get('template', ['aprende-con-nosotros', function()
{
	return View::make('derco.page.aprende');
}]);

Route::get('template', ['concesionarios', function()
{
	return View::make('derco.mixing.template');
}]);

Route::get('404', function()
{
	return View::make('derco.page.404');
});

Route::get('template', ['clinica-2018', function()
{
	return View::make('derco.clinica.step1');
}]);

Route::get('template', ['paso2', function()
{
	if (!isset($_SESSION['clinic']['rut'])) {
		header('Location:http://www.dercomotos.cl/clinica-de-manejo');
	}
	return View::make('derco.clinica.step2');
}]);

Route::get('template', ['paso3', function()
{	
	if (!isset($_SESSION['clinic'])) {
		header('Location:http://www.dercomotos.cl/clinica-de-manejo');
	}
	return View::make('derco.clinica.step3');
}]);

Route::get('template', ['edit', function()
{
	if (!isset($_SESSION['edit'])) {
		header('Location:http://www.dercomotos.cl/clinica-de-manejo');
	}
	return View::make('derco.clinica.edit');

}]);

