<?php

Ajax::run('step1', 'both', function() {
	
	extract($_POST);
	
	global $wpdb;

	$fecha = get_field('clinic_date', 1084);

	$exists = $wpdb->query('SELECT id FROM wp_clinic_inscriptions WHERE rut = "'.$rut.'" AND inscription_date ="'.$fecha.'"');


	$_SESSION['clinic'] = [
		'firstname' => $firstname,
		'lastname' 	=> $lastname,
		'rut'		=> $rut,
		'email'		=> $email,
		'phone'		=> $phone
	];
	
	if (!empty($_SESSION['clinic']['firstname']) && !$exists) {
		$response = [
	  		"status" => true,
		  	"redirect" => 'paso-2',
		];
	} elseif ($exists) {
		$response = [
	  		"status" => false,
		  	"message" => [
	    		"title" => "RUT duplicado",
	    		"text" => "Ya existe una reserva con este RUT."
	  		]
		];
	} else {
		$response = [
	  		"status" => false,
		  	"message" => [
	    		"title" => "Error",
	    		"text" => "A ocurrido un error."
	  		]
		];
	}

	wp_send_json($response);
});

Ajax::run('step2', 'both', function() {

	global $wpdb;
	
	$table = $wpdb->prefix . 'clinic_inscriptions';

	extract($_SESSION['clinic']);
	extract($_POST);
	
	/* $firstname
	 * $lastname
	 * $rut
	 * $email
	 * $phone
	 * $time
	 * $date
	 * $id
	 */

	
	$time = explode('.', $time);

	$array_data = [
		'name' 				=> $firstname,
		'lastname' 			=> $lastname,
		'rut'				=> $rut,
		'email'				=> $email,
		'phone'				=> $phone,
		'inscription_date'	=> $date,
		'inscription_hour'	=> $time[1],
		'date_field'		=> 'clinic_shedule_'.$time[0].'_used',
	];

	// Save to DB
	$insert = $wpdb->insert($table, $array_data);

	$current = 0;

	if ($insert) {
		// foreach (get_field('clinic_shedule', $id) as $key =>  $schedule) {
		// 	if ($key == $time[0]) {
		// 		$current = $schedule['used'];
		// 	}
		// }
		// $current++;
		$query = 'UPDATE wp_postmeta SET meta_value = meta_value - 1 WHERE post_id = '.$id.' AND meta_key = "clinic_shedule_'.$time[0].'_used"';
		$update = $wpdb->query($query);
	}

	// Mandrill
	$mandrill = new Mandrill('anCnzLBpxLE5XvzJw3Togg');

	$vars_for_template = [
		'FORMULARIO'	=> 'Clinica de Manejo',
		'FIRSTNAME' 	=> $array_data['name'],
		'RUT' 			=> $array_data['rut'],
		'EMAIL_USER' 	=> $array_data['email'],
		'PHONE' 		=> $array_data['phone'],
		'DATE'			=> $array_data['inscription_date'],
		'HOUR'			=> $array_data['inscription_hour']	,
		'MENSAJE'		=> 'Inscripción clinica de manejo',
		'PLACE'			=> 'Autoshopping Av. Departamental 4500, Macul',
	];

	$message = [
		'from_email' 		  => 'no-reply@dercomotos.cl',
		'from_name' 		  => 'DERCO Motos',
		'subject' 			  => 'Clinica de manejo DERCO Motos',
		'preserve_recipients' => FALSE,
	];

	// $email = 'fmorano@meat.cl';
	$email = $email;
		
	$message['to'] = [
		['email' => $email],
	];

	foreach($vars_for_template as $key => $value):
		$message["global_merge_vars"][] = [
			'name' => $key,
			'content' => $value
		];
	endforeach;

	#Send Mail
	$mandrill->messages->sendTemplate('derco-formularios', [] ,$message);

	$_SESSION['clinic']['time'] = $time[1];
	$_SESSION['clinic']['date'] = $date;
	$_SESSION['clinic']['id'] = $wpdb->insert_id;


	$response = [
  		"status" => true,
	  	"redirect" => '/clinica-de-manejo/paso-3',
	];

	wp_send_json($response);

});

Ajax::run('edit', 'both', function() {
	extract($_POST);

	global $wpdb;
	
	$table = $wpdb->prefix . 'clinic_inscriptions';

	$fecha = get_field('clinic_date', 1084);

	$query = "SELECT * FROM ".$table." WHERE rut = '".$rut."' AND inscription_date ='".$fecha."'";

	$data = $wpdb->get_results($query);

	if (!empty($data)) {
		$_SESSION['edit'] = $data['0'];

		$response = [
  			"status" => true,
	  		"redirect" => '/clinica-de-manejo/edit',
		];
	} elseif(empty($data)) {
		$response = [
	  		"status" => false,
		  	"message" => [
	    		"title" => "",
	    		"text" => "No existe registro de este RUT para la fecha actual de la Clinica",
	  		]
		];
	} else {
		$response = [
	  		"status" => false,
		  	"message" => [
	    		"title" => "Error",
	    		"text" => "EL RUT que ha ingresado no existe",
	  		]
		];
	}

	wp_send_json($response);
});

Ajax::run('editschedule', 'both', function() {
	global $wpdb;

	$table = $wpdb->prefix . 'clinic_inscriptions';

	extract($_POST);

	$time = explode('.', $time);

	$queryOld = 'UPDATE wp_postmeta SET meta_value = meta_value + 1 WHERE post_id = 1084 AND meta_key = "'.$fields.'"';
	$queryNew = 'UPDATE wp_postmeta SET meta_value = meta_value - 1 WHERE post_id = 1084 AND meta_key = "clinic_shedule_'.$time[0].'_used"';
	$queryUser = 'UPDATE '.$table.' SET inscription_hour = "'.$time[1].'", date_field = "clinic_shedule_'.$time[0].'_used" WHERE id = '.$id;

	$fieldsUpdateOld = $wpdb->query($queryOld);

	$fieldsUpdateNew = $wpdb->query($queryNew);

	$fieldsUpdateUser = $wpdb->query($queryUser);

	if ($fieldsUpdateNew && $fieldsUpdateOld && $fieldsUpdateUser) {

		$_SESSION['edit']->inscription_hour = $time[1];
		$_SESSION['edit']->date_field = 'clinic_shedule_'.$time[0].'_used';

		$_SESSION['edit_success'] = 'Tus datos han sido actulizados existosamente';

		$response = [
	  		"status" => true,
		  	"redirect" => '/clinica-de-manejo/edit',
		];
	} else {
		$response = [
	  		"status" => false,
		  	"message" => [
	    		"title" => "Error",
	    		"text" => "No se ha podido actulizar la fecha de tu agendamiento",
	  		]
		];
	}

	wp_send_json($response);

});

Ajax::run('edituser', 'both', function() {
	global $wpdb;

	$table = $wpdb->prefix . 'clinic_inscriptions';

	extract($_POST);

	$array_data = [
		'name' 				=> $firstname,
		'lastname' 			=> $lastname,
		'rut'				=> $rut,
		'email'				=> $email,
		'phone'				=> $phone,
	];

	// // Update row
	$query = 'UPDATE '.$table.' SET name = "'.$firstname.'", lastname = "'.$lastname.'", rut = "'.$rut.'", email = "'.$email.'", phone = "'.$phone.'" WHERE id = '.$id;

	$update = $wpdb->query($query);

	if ($update) {

		$_SESSION['edit']->name = $firstname; 
		$_SESSION['edit']->lastname = $lastname; 
		$_SESSION['edit']->rut = $rut; 
		$_SESSION['edit']->email = $email;
		$_SESSION['edit']->phone = $phone; 

		$_SESSION['edit_success'] = 'Tus datos han sido actulizados existosamente';

		$response = [
	  		"status" => true,
		  	"redirect" => '/clinica-de-manejo/edit',
		];
		
	} else {
		$response = [
	  		"status" => false,
		  	"message" => [
	    		"title" => "Error",
	    		"text" => "No se han podido actulizar tus datos.",
	  		]
		];
	}

	wp_send_json($response);

});

Ajax::run('deleteclinic', 'both', function() {
	global $wpdb;

	$table = $wpdb->prefix . 'clinic_inscriptions';

	$deleteQuery = "DELETE FROM ".$table." WHERE rut = '".$_SESSION['edit']->rut."'";

	$delete = $wpdb->query($deleteQuery);
	$revert = $wpdb->query('UPDATE wp_postmeta SET meta_value = meta_value + 1  WHERE post_id = 1084 AND meta_key = "'.$_SESSION['edit']->date_field.'"');

	if ($delete && $revert) {

		unset($_SESSION['edit']);
		
		$_SESSION['delete_success'] = 'Tus datos han sido eliminados con exito';
		
		$response = [
	  		"status" => true,
		  	"redirect" => '/clinica-de-manejo?delete=true',
		];
	} else {
		$response = [
	  		"status" => false,
		  	"message" => [
	    		"title" => "Error",
	    		"text" => "No se han podido eliminar tus datos.",
	  		]
		];
	}

	wp_send_json($response);
});
