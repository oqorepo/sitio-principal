<?php

class helpers{

	public static function class_big($i){
		$big = '';
		if(round(($i/6), 2) == 0.67 || $i % 6 == 0)
			$big = 'big';
		return $big;
	}

	public static function ajax_url(){
		return get_site_url() . '/wp-admin/admin-ajax.php';
	}

	public static function slugify($str,$delimiter){
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		return $clean;
	}

	public static function randomString($quantity){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $quantity; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

}