<?php

if(strstr($_SERVER['REQUEST_URI'], 'wp-admin/post-new.php') || strstr($_SERVER['REQUEST_URI'], 'wp-admin/post.php')) {

	ob_start('one_category_only');
}

function one_category_only($content) {

	$content = str_replace('type="checkbox" name="tax_input[estado-proyectos][]"', 'type="radio" name="tax_input[estado-proyectos][]"', $content);
	$content = str_replace('href="#tipo-proyectos-add"', 'style="display: none;"', $content);

	return $content;

}