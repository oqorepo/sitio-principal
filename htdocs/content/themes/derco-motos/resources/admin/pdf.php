<?php

	if(isset($_GET['pdf'])){
		ob_start();

		if(SITE_NAME == 'suzuki'){
			$url_logo = 'https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/13592ac2-7f97-4f2b-8b17-14d6cbc212c4.png';
			$width_logo = '120';
			$color_top = 'white';
			$color_bottom = '#3a66c8';
		} elseif(SITE_NAME == 'zongshen') {
			$url_logo = 'https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/54d0b4a4-f139-48df-b9fe-7306a12c7551.png';
			$width_logo = '250';
			$color_top = '#ff0000';
			$color_bottom = '#ff0000';
		} elseif(SITE_NAME == 'kymco') {
			$url_logo = 'https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/7603e24a-e87b-4bb6-b257-fa8244af5fca.png';
			$width_logo = '250';
			$color_top = 'white';
			$color_bottom = '#d00429';
		} else {
			$url_logo = 'https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/394198e5-71ab-42de-b997-734079c306d0.png';
			$width_logo = '200';
			$color_top = '#ee1a2e';
			$color_bottom = '#ee1a2e';
		}

		include ABSPATH.'../content/themes/derco-motos/pdf-template/quote.php';
		
		function generateRandomString($length = 10) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
		$content = ob_get_clean();
		$html2pdf = new Html2Pdf('P', 'LETTER', 'es');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content);
		$html2pdf->Output(ABSPATH . '../pdf/' . generateRandomString(20) . '.pdf', 'F');
		die();
	}