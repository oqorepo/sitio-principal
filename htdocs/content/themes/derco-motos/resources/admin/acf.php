<?php
/**
 * ACF has this fancy option to show ACF fields based on pages templates.
 * One problem: Themosis templates are not registered as "default" WordPress templates.
 * This file hooks into the acf filters and allows ACF to use the Themosis templates.
 */
// add themosis templates
add_filter('acf/location/rule_values/page_template', 'add_themosis_templates_to_acf_rules');
function add_themosis_templates_to_acf_rules($choices)
{
	$key = 'theme';
	$configFile = 'templates.config.php';
	$configTemplates = include(themosis_path($key) . 'config' . DS . $configFile );
	$templates = array();
	foreach ($configTemplates as $configTemplate) {
		$prettyTemplateName = str_replace(array('-', '_'), ' ', ucfirst(trim($configTemplate)));
		$templates[$configTemplate] = $prettyTemplateName;
	}
	return array_merge(array('none' => __('- None -')), $templates);
}
// get themosis templates
add_filter('acf/location/rule_match/page_template', 'get_themosis_templates_from_acf_rules', 11, 3);
function get_themosis_templates_from_acf_rules($match, $rule, $options)
{
	// vars
	$page_template = $options['page_template'];
	// get page template
	if (!$page_template && $options['post_id']) {
		$page_template = get_post_meta($options['post_id'], '_themosisPageTemplate', true);
	}
	// compare
	if ($rule['operator'] == "==") {
		$match = ($page_template === $rule['value']);
	} elseif ($rule['operator'] == "!=") {
		$match = ($page_template !== $rule['value']);
	}
	// return
	return $match;
}


	add_filter('acf/load_value/key=field_573e165b64c07',  'afc_load_my_repeater_value', 10, 3);
	function afc_load_my_repeater_value($value, $post_id, $field) {
		if ( get_post_status($post_id) === 'auto-draft' ) {

			$value = [
				[
					'field_573e167464c08' => 'Motor',
					'field_573e168664c09' => [
						['field_573e16a464c0a' => 'Cilindrada'],
						['field_573e16a464c0a' => 'N° de cilindros'],
						['field_573e16a464c0a' => 'Compresión'],
						['field_573e16a464c0a' => 'Sist. Comb.'],
						['field_573e16a464c0a' => 'Compresión'],
						['field_573e16a464c0a' => 'Refrigeración'],
						['field_573e16a464c0a' => 'Sistema Partida'],
						['field_573e16a464c0a' => 'Transmisión'],
						['field_573e16a464c0a' => 'Embrague'],
						['field_573e16a464c0a' => 'Tanque Combustible']
					]
				],
				[
					'field_573e167464c08' => 'Dimensiones',
					'field_573e168664c09' => [
						['field_573e16a464c0a' => 'Modelo'],
						['field_573e16a464c0a' => 'Largo'],
						['field_573e16a464c0a' => 'Ancho'],
						['field_573e16a464c0a' => 'Alto'],
						['field_573e16a464c0a' => 'Altura del asiento'],
						['field_573e16a464c0a' => 'Dist. entre ejes'],
						['field_573e16a464c0a' => 'Peso']
					]
				],
				[
					'field_573e167464c08' => 'Frenos',
					'field_573e168664c09' => [
						['field_573e16a464c0a' => 'Delantero'],
						['field_573e16a464c0a' => 'Trasero']
					]
				],
				[
					'field_573e167464c08' => 'Suspensión',
					'field_573e168664c09' => [
						['field_573e16a464c0a' => 'Susp. delantera'],
						['field_573e16a464c0a' => 'Susp. trasera']
					]
				],
				[
					'field_573e167464c08' => 'Dimensiones',
					'field_573e168664c09' => [
						['field_573e16a464c0a' => 'Modelo'],
						['field_573e16a464c0a' => 'Largo'],
						['field_573e16a464c0a' => 'Ancho'],
						['field_573e16a464c0a' => 'Alto'],
						['field_573e16a464c0a' => 'Altura del asiento'],
						['field_573e16a464c0a' => 'Dist. entre ejes'],
						['field_573e16a464c0a' => 'Peso']
					]
				],
				[
					'field_573e167464c08' => 'Neumáticos',
					'field_573e168664c09' => [
						['field_573e16a464c0a' => 'Delantero'],
						['field_573e16a464c0a' => 'Trasero']
					]
				]
			];

		}
		return $value;
	}