<?php

	session_start();
	
	if(!isset($_SESSION['user'])){
		$_SESSION['user'] = [
			'firstname'     => '',
			'lastname'      => '',
			'rut'           => '',
			'email'         => '',
			'phone'         => '',
			'address'       => '',
			'region_personal' => '',
			'comuna_personal' => ''
		];
	}

	if(isset($_GET['model'])){
		setcookie("model_select", $_GET['model']);
	}
