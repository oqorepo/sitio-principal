<?php

	Ajax::run('concesionarios', 'both', function() {

		$query = new WP_Query_Multisite(['post_type' => 'page', 'sites' => ['sites__in' => [1]]]);

		while ($query->have_posts()) : $query->the_post();
			if (get_post()->post_name == 'concesionarios') :
				while (have_rows('region_zn')): the_row();
					if($_GET['id'] == helpers::slugify(get_sub_field('nombre_region_zn'), '-')){
						$nombre_comuna_ant[] =  '';
						$i = 1;

						while (have_rows('comunas_zn')) { the_row();
							$nombre_comuna = get_sub_field('nombre_comuna_zn');

							while (have_rows('concesionarios_zn')) { the_row();

								$marca_actual = false;

								foreach (get_sub_field('marcas') as $marca) {
									if ($marca == SITE_NAME)
										$marca_actual = true;
								}

								$nombre_comuna_comunas[0] =
								[
									'id' => '',
									'name' => 'Seleccione su comuna'
								];
								if ($marca_actual) {
									$nombre_comuna_ant[] = $nombre_comuna;
									if ($nombre_comuna_ant[count($nombre_comuna_ant) - 2] != $nombre_comuna) {
										$nombre_comuna_comunas[$i] =
											[
												'id' => helpers::slugify($nombre_comuna, '-'),
												'name' => $nombre_comuna
											];
										$i++;
									}
								}
							}
						}

					} else {

						$marca_actual = false;

						while (have_rows('comunas_zn')) { the_row();

							$nombre_comuna = get_sub_field('nombre_comuna_zn');

							if(helpers::slugify($nombre_comuna, '-') == $_GET['id']){
								while (have_rows('concesionarios_zn')) { the_row();
									# Fix campo de concesionarios
									$marca_actual = false;
									foreach (get_sub_field('marcas') as $marca) {
										if ($marca == SITE_NAME)
											$marca_actual = true;
									}

									if ($marca_actual) {
										$value_concesionario = isset(get_field('envio_a_ces')[0]) ? get_sub_field('email') . '|' . get_sub_field('nombre_concesinario') . '|' . get_sub_field('email_cc_1') . '|' . get_sub_field('email_cc_2') . '|' . get_sub_field('email_cc_3')  : ' |' . get_sub_field('nombre_concesinario');
										if(get_sub_field('nombre_concesinario') != 'Nacional Motos') {
											$concecionarios[] =  [
												'id'   => $value_concesionario,
												'name' => get_sub_field('nombre_concesinario'),
												'option' => get_field('envio_a_ces'),
												'use_autofin' => get_sub_field('use_autofin'),
												'id_autofin' => get_sub_field('id_autofin'),
											];
										}
									}

								}
							}
						}
					}
				endwhile;
			endif;
		endwhile;

		if(isset($_GET['type']) && $_GET['type'] == 'comuna'){
			$return['fields'] = $nombre_comuna_comunas;
		} else {
			$return['fields'] = $concecionarios;
		}

		wp_send_json($return);
		wp_reset_postdata();

	});