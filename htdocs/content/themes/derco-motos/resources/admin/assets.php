<?php

    $version_front = '5.4.9';
    
    Asset::add('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', false, $version_front, true);
    //Asset::add('app-fonts-css', 'fonts/stylesheet.css', false, '0.3', 'all');
    Asset::add('app-css', 'app.min.css', false, $version_front, 'all');

    Asset::add('maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyAtCl9KwsDj9abVk-ORpT5GRtAsePghcs0&.js', false, '1.0.7', 'all');
    Asset::add('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', true, '1.11.2', 'all');
    Asset::add('jquery-ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', false, '1.11.3', 'all');
    Asset::add('app-js', 'app.min.js', false, $version_front, 'all');

    ### Disable jQuery Migrate
    add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );
    function dequeue_jquery_migrate( &$scripts){
        if(!is_admin()){
            $scripts->remove( 'jquery');
            $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
        }
    }
    
    