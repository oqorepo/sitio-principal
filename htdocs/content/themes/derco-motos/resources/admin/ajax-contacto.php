<?php

Ajax::run('contacto', 'both', function () {

    global $wpdb;
    
    $mandrill = new Mandrill('anCnzLBpxLE5XvzJw3Togg');
    $table = $wpdb->prefix . 'contact';

    if (!isset($_POST['fullname'])) {
        $_POST['fullname'] = $_POST['firstname'] .' '. $_POST['lastname'];
    }

    $array_data = [
        'fullname'  => $_POST['fullname'],
        'rut'       => $_POST['rut'],
        'email'     => $_POST['email'],
        'phone'     => substr($_POST['phone'], -9),
        'message'   => $_POST['message']
    ];


    $message = [
        'from_email'          => 'no-reply@dercomotos.cl',
        'from_name'           => 'DERCO Motos',
        'subject'             => 'Contacto Web DERCO Motos',
        'preserve_recipients' => false,
    ];

    if (SITE_NAME == 'royal') {
        $message['to'] = [
            ['email' => 'ccenter@derco.cl']
        ];
    } else {
        $message['to'] = [
            ['email' => 'cristobalmorgan@derco.cl']
        ];
    }

    

    $vars_for_template = [
        'FORMULARIO'    => 'Contacto',
        'FIRSTNAME' => $array_data['fullname'],
        'RUT' => $array_data['rut'],
        'EMAIL_USER' => $array_data['email'],
        'PHONE' => $array_data['phone'],
        'MENSAJE' => $array_data['message'],
    ];

    foreach ($vars_for_template as $key => $value) :
        $message["global_merge_vars"][] = [
            'name' => $key,
            'content' => $value
        ];
    endforeach;

    #Save in DB
    $wpdb->query(query::insert($table, $array_data));

    #Send Mail
    $mandrill->messages->sendTemplate('derco-formularios', [], $message);


    $response = [
        'status' => true,
        'message' => [
            'title' => $_POST['fullname'] .', gracias por contactarnos',
            'text' => $_POST['send_text']
        ]
    ];

    wp_send_json($response);

});
