<?php

ob_start('clear_title');

function clear_title($content) {

	$actual_title =  between('<title>', '</title>', $content);
	$new_title =  between('<title >', '</title>', $content);

	if(strstr($actual_title, 'Page not found')){
		$content = str_replace('Page not found', $new_title, $content);
		$content = str_replace('<title >' . $new_title . '</title>', '', $content);
	}

	return $content;

}