<?php

Ajax::run('clinica', 'both', function () {

    global $wpdb;
    
    $mandrill = new Mandrill('anCnzLBpxLE5XvzJw3Togg');
    $table = $wpdb->prefix . 'clinic';

    if (!isset($_POST['fullname'])) {
        $_POST['fullname'] = $_POST['firstname'] .' '. $_POST['lastname'];
    }

    $array_data = [
        'fullname'  => $_POST['fullname'],
        'rut'       => $_POST['rut'],
        'email'     => $_POST['email'],
        'phone'     => substr($_POST['phone'], -9),
        'schedule'  => $_POST['schedule'] ? $_POST['schedule'] : '',
    ];


    $message = [
        'from_email'          => 'no-reply@dercomotos.cl',
        'from_name'           => 'DERCO Motos',
        'subject'             => 'Clinica de manejo DERCO Motos',
        'preserve_recipients' => false,
    ];

    if ($_POST['email'] == 'fmorano@meat.cl') {
        $message['to'] = [
            ['email' => 'fmorano@meat.cl'],
        ];
    } else {
        $message['to'] = [
            ['email' => 'cristobalmorgan@derco.cl'],
        ];
    }

    $vars_for_template = [
        'FORMULARIO'    => 'Escuela de Manejo',
        'FIRSTNAME' => $array_data['fullname'],
        'RUT' => $array_data['rut'],
        'EMAIL_USER' => $array_data['email'],
        'PHONE' => $array_data['phone'],
        'MENSAJE'   => 'Inscripción clinica de manejo</span></td>
        </tr><tr>
            <td style="text-align: left;"><span style="font-size:12px">Horario</span></td>
            <td><span style="font-size:12px">:&nbsp;'.$array_data['schedule'],
    ];

    foreach ($vars_for_template as $key => $value) {
        $message["global_merge_vars"][] = [
            'name' => $key,
            'content' => $value
        ];
    }

    #Save in DB
    $wpdb->query(query::insert($table, $array_data));

    #Send Mail
    $mandrill->messages->sendTemplate('derco-escuela', [], $message);


    $response = [
        'status' => true,
        'message' => [
            'title' => $_POST['fullname'] .', gracias por contactarnos',
            'text' => $_POST['send_text']
        ]
    ];

    wp_send_json($response);
});
