<?php

#Disable emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

// Editores pueden administrar menús:
function edit_editor_menus(){
	$role_object = get_role('editor');
	$role_object->add_cap('edit_theme_options');
}

add_action('admin_menu','edit_editor_menus');

function remove_menus(){
  $currentUser = wp_get_current_user();

  	if ($currentUser->ID == 18) {
		remove_menu_page( 'index.php' );                  //Dashboard
		remove_menu_page( 'edit.php' );                   //Posts
		remove_menu_page( 'upload.php' );                 //Media
		remove_menu_page( 'edit.php?post_type=page' );    //Pages
		remove_menu_page( 'edit.php?post_type=acf-field-group' );    //Pages
		remove_menu_page( 'edit-comments.php' );          //Comments
		remove_menu_page( 'themes.php' );                 //Appearance
		remove_menu_page( 'plugins.php' );                //Plugins
		remove_menu_page( 'users.php' );                  //Users
		remove_menu_page( 'tools.php' );                  //Tools
		remove_menu_page( 'options-general.php' );        //Settings
		remove_menu_page( 'opciones-transversales' );        //Settings
		remove_menu_page( 'acf-field-group' );        //Settings
		remove_menu_page( 'wpseo_dashboard' );        //Settings
		remove_menu_page( 'customtaxorder' );        //Settings
		remove_menu_page( 'descarga-bbdd' );        //Settings
		remove_menu_page( 'csv-cotizaciones' );        //Settings
	} elseif ($currentUser->ID == 19) {
		remove_menu_page( 'index.php' );                  //Dashboard
		remove_menu_page( 'edit.php' );                   //Posts
		remove_menu_page( 'upload.php' );                 //Media
		remove_menu_page( 'edit.php?post_type=page' );    //Pages
		remove_menu_page( 'edit.php?post_type=acf-field-group' );    //Pages
		remove_menu_page( 'edit-comments.php' );          //Comments
		remove_menu_page( 'themes.php' );                 //Appearance
		remove_menu_page( 'plugins.php' );                //Plugins
		remove_menu_page( 'users.php' );                  //Users
		remove_menu_page( 'tools.php' );                  //Tools
		remove_menu_page( 'options-general.php' );        //Settings
		remove_menu_page( 'opciones-transversales' );        //Settings
		remove_menu_page( 'acf-field-group' );        //Settings
		remove_menu_page( 'wpseo_dashboard' );        //Settings
		remove_menu_page( 'customtaxorder' );        //Settings
		remove_menu_page( 'descarga-clinica' );        //Settings
		remove_menu_page( 'csv-cotizaciones' );        //Settings
	}
 
}
add_action( 'admin_init', 'remove_menus' );

