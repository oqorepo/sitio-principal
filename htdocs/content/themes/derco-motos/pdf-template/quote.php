<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cotización</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        .space{
            height: 48px;
        }
    </style>
</head>
<body style="position: relative">
    <div id="title" style="width: 100%; background: <?php echo $color_top ?>; text-align: center; padding: 20px; box-sizing: border-box">
        <img src="<?php echo $url_logo ?>" width="<?php echo $width_logo ?>">
    </div>
    <div class="space"></div>
    <div class="text" style=" width:100%; text-align: center; padding:0 100px"><span style="font-size:24px"><em><strong>&iexcl;Gracias por cotizar con <?php echo ucfirst(SITE_NAME) ?> motos!</strong></em></span><br />
        <br />
        <span style="font-size:14px">Pronto ser&aacute;s contactado por un ejecutivo que ayudar&aacute; a complementar y resolver cualquier duda adicional que tengas sobre tu cotizaci&oacute;n.</span></div>
    <div class="space"></div>
        <table border="0"  style="width: 530px;"  cellspacing="15">
            <tbody>
            <tr>
                <td width="100"></td>
                <td style="text-align: left; width: 50%"><span style="font-size:18px">Nombre</span></td>
                <td style="width: 50%"><span style="font-size:18px">: <?php echo $array_data['firstname'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Apellido</span></td>
                <td><span style="font-size:18px">: <?php echo $array_data['lastname'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Rut</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo $array_data['rut'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">E-mail</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo $array_data['email'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Tel&eacute;fono</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo $array_data['phone'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Direcci&oacute;n</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo $array_data['address'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Comuna</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo $array_data['comuna_personal'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Regi&oacute;n</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo $array_data['region_personal'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Marca</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo strtoupper(SITE_NAME) ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Modelo</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo empty($array_data['model_number']) ? $array_data['model'] : $array_data['model'] . ' (' . $array_data['model_number'] . ')'?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Nombre concesionario</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo $array_data['concessionaire_name'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Comuna concesionario</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo $array_data['concessionaire_comuna'] ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Valor vehiculo</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo format::number($array_data['value_vehicle']) ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Bono</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo format::number($array_data['value_bono']) ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">Valor total</span></td>
                <td><span style="font-size:18px">:&nbsp;<?php echo format::number($array_data['value_total']) ?></span></td>
                <td width="100"></td>
            </tr>
            <tr>
                <td width="100"></td>
                <td style="text-align: left;"><span style="font-size:18px">URL en cotizador</span></td>
                <td><span style="font-size:18px">:&nbsp;<a href="<?php echo $array_data['url_cotizacion'] ?>" target="_blank"><?php echo $array_data['url_cotizacion'] ?></a></span></td>
                <td width="100"></td>
            </tr>
            </tbody>
        </table>



    <div class="mcnTextContent" style="background: <?php echo $color_bottom ?>; text-align: center; padding: 10px 0; position: absolute; bottom: 0; width: 100%">
                <a href="https://www.facebook.com/DERCOMOTOS" target="_blank"><img src="https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/231e20c6-374a-4301-aa6a-dd1797da9c8d.png" style="width: 25px"></a>
                &nbsp;
                <a href="https://twitter.com/dercomotos" target="_blank"><img src="https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/288dc8a5-aa8e-4225-bc1e-c924eb528fd8.png" style="width: 25px"></a>
                &nbsp;
                <a href="https://www.instagram.com/dercomotos/" target="_blank"><img src="https://gallery.mailchimp.com/8f2c33167f17a54a4ba1c5665/images/00d77945-bdc6-4163-8930-12953c52fe33.png" style="width: 25px"></a>

    </div>


</body>
</html>