<?php

/*----------------------------------------------------*/
// Database
/*----------------------------------------------------*/
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
$table_prefix = getenv('DB_PREFIX') ? getenv('DB_PREFIX') : 'wp_';

/*----------------------------------------------------*/
// Authentication unique keys and salts
/*----------------------------------------------------*/
/**
 * @link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service 
 */
define('AUTH_KEY',         'DXRcL~&|9*I|4&4+JV|=9YMbb9-o;Dr_RdWd9E2J-n7+S3$_$f8aYRRWu;3uORv=');
define('SECURE_AUTH_KEY',  '3]%]01kNR<id+LTmLvD-8mO{ld[1NL)I.8CU^]M.+0in-Q%lCq0)i%w$ %r[P2~6');
define('LOGGED_IN_KEY',    '_F+l<[-3VS{DDGv4_5_D!;~k(N6-|I9~b@{0rZ80`a31|iGAz/<`q5H|4=7H>2tj');
define('NONCE_KEY',        'Mb2j(U+_EFnZ2+*kJO-4`<@&e)(e2EBa+:<Z9f7z^9-jLp=t}hRkAt{G~XtftKz@');
define('AUTH_SALT',        'H>Bg/KY( Z?LV4g4_WQfL*|Yk%H|U=M1G(_~u>D&6t#^QTa-;#C:esy#0=#.3n$j');
define('SECURE_AUTH_SALT', '#]k*mVn@R=<,+m:F-k_|CrCn#9*x2C9^tF+oH~hb-B]CX58wfZ,5wc3r+b[,J-d,');
define('LOGGED_IN_SALT',   '~MX$P%O)g$q4?*8]}_U&0>5O9rpTa:af?&B!-nPv-Jrbb{k:#_@+t)f+K]s8;uHM');
define('NONCE_SALT',       '+rh~Y^@#`xZ+lUsew9Ir3fc%:wjv;?nUJ4mh42u*1(n*w_kFzX+Ji!ynmKR1z->+');

/*----------------------------------------------------*/
// Custom settings
/*----------------------------------------------------*/
define('WP_AUTO_UPDATE_CORE', true);
define('DISALLOW_FILE_EDIT', true);

/*----------------------------------------------------*/
// Multisite Option
/*----------------------------------------------------*/
define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
define('DOMAIN_CURRENT_SITE', getenv('DOMAIN_CURRENT_SITE'));